import sys

from lexer.source import Source
from lexer.lexer import Lexer
from parser.parser import Parser
from interpreter.intepreter import Interpreter

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Nie podano ścieżki do pliku")
    else:
        with open(sys.argv[1], "r") as file:
            lexer = Lexer(Source(file))
            parser = Parser(lexer)
            parser.get_next_token()
            program = parser.try_parse()
            Interpreter(program).interpret()
