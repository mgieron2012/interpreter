# Dokumentacja


## Opis użytkowy

### Koncepcja języka
Język służy do łatwego generowania kodu HTML. Składnia przypomina język Python.
Obiekty Element reprezentują znaczniki html, a budowanie drzewa DOM polega na tworzeniu hierarchii funkcją addChild.
Obiekt Attribute reprezentuje atrybut znacznika. Można go przypisać do obiektu Element funkcją addAttribute.


### Elementy języka

#### Zmienne i ich typy

```python
liczba = 10.5
napis = "Hello world"
bool = True or False
lista = [10, True, False]
słownik = {a: 10, b: "10"}
element = Element("div")
atrybut = Attribute("style", "margin: '10px'")
obiekt_listy = lista[0]
obiekt_słownika = słownik.a
# komentarz;
```
* typowanie dynamiczne
* brak zmiennych globalnych 

#### Definicja funkcji
```python
def Main(){
    html = Element("html");
    return html;
};

def funkcjaArgumentowa(x, y){
    return x + y;
};

def funkcjaArgumentowaDomyślna(x, y=5){
    return x - y;
};
```
* Program musi zawierać funkcję Main, która jest jego punktem startowym. Powinna ona zwracać obiekt Element.

#### Wywołanie funkcji
```python
funkcjaArgumentowa(5, 10);
funkcjaArgumentowaDomyślna(5);
funkcjaArgumentowaDomyślna(5, 10);
```
* Domyślnie argumenty przekazywane są do funkcji przez referencję. Aby przekazać kopię obiektu do funkcji należy użyć funkcji copy.
```python
x = 1;
przekazaniePrzezReferencje(x);
przekazanieKopii(copy(x));
```

#### Instrukcja warunkowa
```python
a = 5;

if(a < 5){
    doSth1();
} 
  
if(a < 10){
    doSth1();     
} else {
    doSth2();    
}
      
if(a < 10){
    doSth2();    
} elif (a < 20){
    doSth3();    
} else {
    doSth4();    
}
```

#### Pętla
```python
a = 0;

for i in [1, 2, 3, 4, 5]{
    a = a + i;
}
```

#### Operatory
```python
mnozenie = 5 * 10
mnozenie2 = "A" * 5
dzielenie = 10 / 2
dzielenie_całkowite = 10 // 3
reszta = 10 % 3
dodawanie = -2 + 3
odejmowanie = 5 - 3
nawiasy = (2 + 2) * 2
operatory_logiczne1 = 5 <= 2
operatory_logiczne2 = 5 < 2
operatory_logiczne3 = 5 >= 2
operatory_logiczne4 = 5 > 2
operatory_logiczne5 = 5 != 2
operatory_logiczne6 = 5 == 2
operator_or = 5 == 2 or 5 != 2 
operator_and = 5 == 2 and 5 != 2
operator_not = not 1 < 3
```

### Przykładowy kod
Generowanie listy nienumerowanej
```python
def ListItem(value){
    result = Element("li");
    addChild(result, value);
    return result;
};

def List(values){
    result = Element("ul");
    for value in values{
        addChild(result, ListItem(value));
    }
    return result;
};

def Style(value)
{
    return Attribute("style", value);
};


def Main()
{
    html = Element("html");
    addAttribute(html, Style("width: 100%"));
    list_elements = ["Element1", "Element2", "Element3"];
    addChild(html, List(list_elements));
    return html;
};
```
Wynik: 
```html
<html style="width: 100%"><ul ><li >Element1</li> <li >Element2</li> <li >Element3</li></ul></html>
```

### Korzystanie z intepretera

Aby zinterpretować kod programu należy uruchomić plik main.py znajdujący się w katalogu głównym projektu: `python3 main.py scieżka_do_pliku_z_kodem > plik_wyjściowy.html`

### Gramatyka

```ebnf
program = { function_def };
block = '{' , { statement }, '}';
statement = assignment | function_def | return_statement | if_statement | for_statement | comment | function;
assignment = NAME, '=', expression, ['if', expression, 'else', expression], ';';

function_def = 'def', NAME, '(', [params], ')', block;
function = NAME, '(', [ expression, {',', expression }], ')';
if_statement = 'if', '(', expression, ')', block, {'elif', '(', expression, ')', block}, ['else', block];
for_statement = 'for', NAME, 'in', expression, block;
return_statement = 'return', expression;

list = '[', [expression, {',', expression }], ']';
dict = '{', [NAME, ':', expression, {',', NAME, ':', expression} '}'];
params = NAME, ['=', expression], { ',', NAME, ['=', expression] };

expression = comparison, {('and' | 'or'), comparison};

comparison = 
    sum |
    sum, '==', sum | 
    sum, '!=', sum |
    sum, '<', sum |
    sum, '<=', sum |
    sum, '>', sum |
    sum, '>=', sum;

sum = ['not'], term, { '+' | '-', term };
term = extended_factor, { '*' | '/' | '//' | '%', extended_factor};
extended_factor = unary_factor, {('.', NAME | function) | ('[', sum, ']')};
unary_factor = ['+' | '-'], factor;
factor = NUMBER | NAME | STRING | function | | ('(', expression, ')') | list | dict | BOOLEAN;

SIGNED_NUMBER = ['-'], NUMBER;
NUMBER = INTEGER, ['.', {digit}];
INTEGER = '0' | (non_zero_digit, { digit });
(* INTEGER = { digit }; *)
BOOLEAN = True | False;
STRING = '"', {letter}, '"';
NAME = letter, { letter | digit | '_'};
digit = '0' | non_zero_digit;
non_zero_digit = '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9';
comment = '#', { letter }, ';';

```

### Opis implementacji

### Lekser
Konstruktor klasy lekser przyjmuje źródło znaków (plik lub napis). Metoda get_next_token() buduje token i go zwraca. 
Klasa zawiera zestaw metod build_X(), które próbują zbudować token konkretnego typu. 

#### Tokeny
- numer linii
- numer kolumny
- typ:
    - identyfikator: wartość
    - stała: wartość, typ (string, number, bool)
    - słowo kluczowe: def, if, elif, else, for, in, return, not, and, or
    - operator addytywny: + -
    - operator multiplikatywny: * / // %
    - operator porównania: < > <= >= == != 
    - operator inny: [ ] { } ( ) . ; : = ,
    - komentarz

### Parser
Konstruktor klasy parser przyjmuje źródło tokenów (Lekser). Metoda try_parse() zwraca obiekt programu.  
Klasa zawiera zestaw metod try_parse_X(), które próbują zbudować obiekt reprezentujący konstrukcję języka.

#### Obiekty
- Funkcja
- Parametry funkcji
- Stała
- Lista
- Słownik
- Wywołanie funkcji
- Negacja
- Indeks listy
- Atrybut słownika
- Operacja
    - porównania
    - oraz, lub
    - dodawanie, odejmowanie
    - mnożenie, dzielenie, reszta z dzielenia, dzielenie całkowite
- Blok
- Operacja powrotu
- Pętla for
- Instrukcja warunkowa if
- Program
- Komentarz
- Przypisanie wartości do zmiennej

### Interpreter
Klasa przyjmuje obiekt programu. Metoda interpret() wypisuje napis reprezentujący obiekt zwrócony przez funkcję Main()
interpretowanego programu. Klasa implementuje wzorzec wizytatora. Zawiera zestaw metod visit_X dla każdego z obiektów 
parsera, które mogą zwracać obiekt klasy Value. Klasa Value zawiera wartość i jej typ. Możliwe typy to: liczba, napis, 
wartość logiczna, lista, słownik, element, atrybut. Interpreter przechowuje listę kontekstów wywołań funkcji, a każdy z 
nich nazwę i listę zakresów, w których znajdują się słowniki z wartościami zmiennych. 


### Testy
- lekser
    - tekst -> token (dla każdego przypadku)
    - tekst -> błąd
- parser:
    - ciąg tokenów -> obiekt
- interpreter + parser + lekser:
    - tekst -> wynik interpretacji dla każdego obiektu
    - tekst -> błąd

### Błędy
- każdy moduł posiada listę błędów
- lekser i parser wyświetlają pozycję błędu w kodzie i tekst błędu
- interpreter wyświetla tekst błędu oraz nazwy funkcji wywołanych przed jego wystąpieniem
- interpreter kończy działanie po napotkaniu pierwszego błędu, lekser i parser nie

Lekser:
- NoEndOfStringError
- NoEndOfCommentError
- UnknownTokenError
- IDTooLongError
- StringTooLongError
- CommentTooLongError
- NumberTooBig
- ExclamationMarkNotFollowedByEquals
- EscapingError
- TrailingZerosError


Interpreter:
- OperationTypesError
- NoMainFunctionError
- NoFunctionError
- VariableNotFoundError
- IndexOutOfRangeError
- IndexOnNonArrayError
- AttributeOnNonDictError
- AttributeNotInDictError
- NonListIteratingError
- ConditionNotBoolError
- DivisionByZeroError
- AssignmentVoidValue
- NotEnoughArgumentsError
- BadArgumentTypeError