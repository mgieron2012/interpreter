from parser.objects import Function, Params


class ElementFunction(Function):
    def __init__(self):
        super().__init__("Element", Params({"name": None}), None)

    def accept(self, visitor):
        return visitor.visit_function_element()


class AttributeFunction(Function):
    def __init__(self):
        super().__init__("Attribute", Params({"name": None, "value": None}), None)

    def accept(self, visitor):
        return visitor.visit_function_attribute()


class AddAttributeFunction(Function):
    def __init__(self):
        super().__init__("addAttribute", Params({"element": None, "attribute": None}), None)

    def accept(self, visitor):
        return visitor.visit_function_add_attribute()


class AddChildFunction(Function):
    def __init__(self):
        super().__init__("addChild", Params({"parent": None, "child": None}), None)

    def accept(self, visitor):
        return visitor.visit_function_add_child()


class CopyFunction(Function):
    def __init__(self):
        super().__init__("copy", Params({"object": None}), None)

    def accept(self, visitor):
        return visitor.visit_function_copy()


class Element:
    def __init__(self, name):
        self.name = name
        self.attributes = []
        self.children = []

    def __str__(self):
        return f"<{self.name} {' '.join([str(attribute) for attribute in self.attributes])}>{' '.join([str(child) for child in self.children])}</{self.name}>"

    def copy(self):
        element = Element(self.name)
        element.attributes = [attr.copy() for attr in self.attributes]
        element.children = [child.copy() for child in self.children]
        return element


class Attribute:
    def __init__(self, name, value):
        self.name = name
        self.value = value

    def __str__(self):
        return f'{self.name}="{self.value}"'

    def copy(self):
        return Attribute(self.name, self.value)
