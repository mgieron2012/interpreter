import io

from interpreter.intepreter import Interpreter, Value
from lexer.lexer import Lexer
from lexer.source import Source
from parser.parser import Parser
import interpreter.errors as errors
import unittest


def code_to_result(code):
    lexer = Lexer(Source(io.StringIO(code)))
    parser = Parser(lexer)
    parser.get_next_token()
    program = parser.try_parse()
    # running visit_program() instead of interpret() to leave errors not caught
    return Interpreter(program).visit_program()


class TestInterpreter(unittest.TestCase):
    def test_main_function(self):
        code = """
        def Main(){
            return Element("html");
        };
        """

        result = code_to_result(code)
        self.assertEquals(result.type.value, Value.Type.ELEMENT.value)
        self.assertEquals(result.value.name.value, "html")

    def test_element_add_child(self):
        code = """
        def Main(){
            html = Element("html");
            div = Element("div");
            addChild(html, div);
            return html;
        };
        """

        result = code_to_result(code)
        self.assertEquals(result.type.value, Value.Type.ELEMENT.value)
        self.assertEquals(result.value.name.value, "html")
        self.assertEquals(result.value.children[0].value.name.value, "div")

    def test_element_add_attribute(self):
        code = """
        def Main(){
            html = Element("html");
            style = Attribute("style", "good");
            addAttribute(html, style);
            return html;
        };
        """

        result = code_to_result(code)
        self.assertEquals(result.type.value, Value.Type.ELEMENT.value)
        self.assertEquals(result.value.name.value, "html")
        self.assertEquals(result.value.attributes[0].value.name.value, "style")
        self.assertEquals(result.value.attributes[0].value.value.value, "good")

    def test_own_function(self):
        code = """
        def Style(value){
            return Attribute("style", value);
        };
        
        def Main(){
            html = Element("html");
            addAttribute(html, Style("good"));
            return html;
        };
        """

        result = code_to_result(code)
        self.assertEquals(result.type.value, Value.Type.ELEMENT.value)
        self.assertEquals(result.value.name.value, "html")
        self.assertEquals(result.value.attributes[0].value.name.value, "style")
        self.assertEquals(result.value.attributes[0].value.value.value, "good")

    def test_default_args(self):
        code = """
        def Style(value, name="style"){
            return Attribute(name, value);
        };

        def Main(){
            html = Element("html");
            addAttribute(html, Style("good"));
            return html;
        };
        """

        result = code_to_result(code)
        self.assertEquals(result.type.value, Value.Type.ELEMENT.value)
        self.assertEquals(result.value.name.value, "html")
        self.assertEquals(result.value.attributes[0].value.name.value, "style")
        self.assertEquals(result.value.attributes[0].value.value.value, "good")

    def test_algebra_add_mul(self):
        code = """        
        def Main(){
            return 2+2*2;
        };
        """

        result = code_to_result(code)
        self.assertEquals(result.type.value, Value.Type.NUMBER.value)
        self.assertEquals(result.value, 6)

    def test_algebra_min_div(self):
        code = """        
        def Main(){
            return 20 - 10 / 2;
        };
        """

        result = code_to_result(code)
        self.assertEquals(result.type.value, Value.Type.NUMBER.value)
        self.assertEquals(result.value, 15)

    def test_algebra_div_int(self):
        code = """        
        def Main(){
            return 20 // 3;
        };
        """

        result = code_to_result(code)
        self.assertEquals(result.type.value, Value.Type.NUMBER.value)
        self.assertEquals(result.value, 6)

    def test_algebra_rest(self):
        code = """        
        def Main(){
            return 20 % 3;
        };
        """

        result = code_to_result(code)
        self.assertEquals(result.type.value, Value.Type.NUMBER.value)
        self.assertEquals(result.value, 2)

    def test_algebra_zero_division(self):
        code = """        
        def Main(){
            return 20 / 0;
        };
        """

        with self.assertRaises(errors.DivisionByZeroError):
            code_to_result(code)

    def test_algebra_zero_rest(self):
        code = """        
        def Main(){
            return 20 % 0;
        };
        """

        with self.assertRaises(errors.DivisionByZeroError):
            code_to_result(code)

    def test_algebra_parenthesis(self):
        code = """        
        def Main(){
            return (1 + 1) * ((2 + 1 / 2) - 2);
        };
        """

        result = code_to_result(code)
        self.assertEquals(result.type.value, Value.Type.NUMBER.value)
        self.assertEquals(result.value, 1)

    def test_bool_algebra_or(self):
        code = """        
        def Main(){
            return True or 1 / 0;
        };
        """

        result = code_to_result(code)
        self.assertEquals(result.type.value, Value.Type.BOOL.value)
        self.assertTrue(result.value)

    def test_bool_algebra_and(self):
        code = """        
        def Main(){
            return False and 1 / 0;
        };
        """

        result = code_to_result(code)
        self.assertEquals(result.type.value, Value.Type.BOOL.value)
        self.assertFalse(result.value)

    def test_bool_algebra_parenthesis(self):
        code = """        
        def Main(){
            return not (1 > 2 or 3 != 4);
        };
        """

        result = code_to_result(code)
        self.assertEquals(result.type.value, Value.Type.BOOL.value)
        self.assertFalse(result.value)

    def test_equality(self):
        code = """        
        def Main(){
            return 1 == 1 and "A" == "A";
        };
        """

        result = code_to_result(code)
        self.assertEquals(result.type.value, Value.Type.BOOL.value)
        self.assertTrue(result.value)

    def test_non_equality(self):
        code = """        
        def Main(){
            return 1 != 1 or "A" != "AB";
        };
        """

        result = code_to_result(code)
        self.assertEquals(result.type.value, Value.Type.BOOL.value)
        self.assertTrue(result.value)

    def test_reference_and_copy(self):
        code = """        
        def Main(){
            x = 0;
            y = x;
            z = copy(x);
            x = x + 1;
            return x == 1 and y == 1 and z == 0;
        };
        """

        result = code_to_result(code)
        self.assertEquals(result.type.value, Value.Type.BOOL.value)
        self.assertTrue(result.value)

    def test_function_return_void_assignment(self):
        code = """
        def Void(){
            x = 1 + 1;
        };
        
        def Main(){
            x = Void();
            return 0;
        };
        """
        with self.assertRaises(errors.AssignmentVoidValue):
            code_to_result(code)

    def test_bad_argument_error(self):
        code = """
        def Main(){
            x = Element(1);
            return x;
        };
        """
        with self.assertRaises(errors.BadArgumentTypeError):
            code_to_result(code)

    def test_not_enough_args_error(self):
        code = """
        def Main(){
            x = Element();
            return x;
        };
        """
        with self.assertRaises(errors.NotEnoughArgumentsError):
            code_to_result(code)

    def test_condition_not_bool_error(self):
        code = """
        def Main(){
            if("A"){
                return 1;
            } 
            return 0;
        };
        """
        with self.assertRaises(errors.ConditionNotBoolError):
            code_to_result(code)

    def test_not_list_iterating_error(self):
        code = """
        def Main(){
            for a in "A"{
                return 1;
            }
        };
        """
        with self.assertRaises(errors.NonListIteratingError):
            code_to_result(code)

    def test_attr_not_in_dict_error(self):
        code = """
        def Main(){
            a = {a: 1, b: 2};
            return a.c;
        };
        """
        with self.assertRaises(errors.AttributeNotInDictError):
            code_to_result(code)

    def test_attr_on_non_dict_error(self):
        code = """
        def Main(){
            a = [1, 2];
            return a.a;
        };
        """
        with self.assertRaises(errors.AttributeOnNonDictError):
            code_to_result(code)

    def test_index_on_non_array_error(self):
        code = """
        def Main(){
            a = {a: 1, b: 2};
            return a[0];
        };
        """
        with self.assertRaises(errors.IndexOnNonArrayError):
            code_to_result(code)

    def test_index_out_of_range_error(self):
        code = """
        def Main(){
            a = [0, 1, 2];
            return a[3];
        };
        """
        with self.assertRaises(errors.IndexOutOfRangeError):
            code_to_result(code)

    def test_variable_not_found_error(self):
        code = """
        def Main(){
            a = 1;
            return b;
        };
        """
        with self.assertRaises(errors.VariableNotFoundError):
            code_to_result(code)

    def test_no_function_error(self):
        code = """
        def Main(){
            a = c();
            return b;
        };
        """
        with self.assertRaises(errors.NoFunctionError):
            code_to_result(code)

    def test_operation_types_error(self):
        code = """
        def Main(){
            return 1 + "1";
        };
        """
        with self.assertRaises(errors.OperationTypesError):
            code_to_result(code)

    def test_list(self):
        code = """
        def List(){
            return [0, 1, 2, "3"];
        };
        
        def Main(){
            return List()[3];
        };
        """

        result = code_to_result(code)
        self.assertEquals(result.type.value, Value.Type.STRING.value)
        self.assertEquals(result.value, "3")

    def test_dict(self):
        code = """
        def Dict(){
            return {
                a: 1,
                b: "3"
            };
        };

        def Main(){
            return Dict().b;
        };
        """

        result = code_to_result(code)
        self.assertEquals(result.type.value, Value.Type.STRING.value)
        self.assertEquals(result.value, "3")

    def test_returning(self):
        code = """
        def A(){
            return 1;
        };
        
        def B(){
            return A();
        };
        
        def C(){
            return B();
            x = 1 / 0;
        };
        
        def Main(){
            return C();
        };
        """

        result = code_to_result(code)
        self.assertEquals(result.type.value, Value.Type.NUMBER.value)
        self.assertEquals(result.value, 1)

    def test_for(self):
        code = """
        def Sum(list){
            x = 0;
            for i in list{
                x = x + i;
            }
            return x;
        };

        def Main(){
            return Sum([1, 2, 3, 4, 5]);
        };
        """

        result = code_to_result(code)
        self.assertEquals(result.type.value, Value.Type.NUMBER.value)
        self.assertEquals(result.value, 15)

    def test_if_else(self):
        code = """
        def Abs(a){
            if (a < 0){
                return -a;
            } else {
                return a;
            }
            x = 1 / 0;
        };

        def Main(){
            return Abs(10) == 10 and Abs(-10) == 10;
        };
        """

        result = code_to_result(code)
        self.assertEquals(result.type.value, Value.Type.BOOL.value)
        self.assertTrue(result.value)

    def test_scopes(self):
        code = """
        def Main(){
            if(True){
                a = 1;
            }
            return a;
        };
        """

        with self.assertRaises(errors.VariableNotFoundError):
            code_to_result(code)

    def test_function_arg_copy(self):
        code = """
        def Increment(a){
            a = a + 1;
        };
        
        def Main(){
            a = 1;
            Increment(a);
            Increment(copy(a));
            return a == 2;
        };
        """

        result = code_to_result(code)
        self.assertEquals(result.type.value, Value.Type.BOOL.value)
        self.assertTrue(result.value)

    def test_returning_value(self):
        code = """
        def A(a){
            return a;
        };
        
        def B(a){
            return A(a);
        };
        
        def C(a){
            B(a);
        };

        def Main(){
            a = C(1);
            return a;
        };
        """

        with self.assertRaises(errors.AssignmentVoidValue):
            code_to_result(code)

    def test_comment(self):
        code = """
        # Komentarz;
        
        def Main(){
        # Komentarz 2;
            return 0;
        };
        """

        result = code_to_result(code)
        self.assertEquals(result.type.value, Value.Type.NUMBER.value)
        self.assertEquals(result.value, 0)
