class InterpreterError(Exception):
    def __init__(self, interpreter):
        self.interpreter = interpreter
        super().__init__(self.get_full_message())

    def get_full_message(self):
        return f"Błąd interpretacji: {self.get_message()}\n{self.get_context_tree()}"

    def get_context_tree(self):
        return "\n|\n".join([context.name for context in self.interpreter.function_call_contexts])

    def get_message(self):
        return "InterpreterError"


class OperationTypesError(InterpreterError):
    def get_message(self):
        return "Operacja przeprowadzona na niepoprawnych typach"


class NoMainFunctionError(InterpreterError):
    def get_message(self):
        return "Brak funkcji Main"


class NoFunctionError(InterpreterError):
    def __init__(self, interpreter, name):
        self.name = name
        super().__init__(interpreter)

    def get_message(self):
        return f"Brak funkcji {self.name}"


class VariableNotFoundError(InterpreterError):
    def __init__(self, interpreter, name):
        self.name = name
        super().__init__(interpreter)

    def get_message(self):
        return f"Brak zainicjowanej zmiennej {self.name}"


class IndexOutOfRangeError(InterpreterError):
    def __init__(self, interpreter, idx):
        self.idx = idx
        super().__init__(interpreter)

    def get_message(self):
        return f"Indeks {self.idx} wykracza poza rozmiar tablicy"


class IndexOnNonArrayError(InterpreterError):
    def get_message(self):
        return "Operacja pobrania indeksu na obiekcie niebędącym listą"


class AttributeOnNonDictError(InterpreterError):
    def get_message(self):
        return "Operacja pobrania atrybutu na obiekcie niebędącym słownikiem"


class AttributeNotInDictError(InterpreterError):
    def __init__(self, interpreter, name):
        self.name = name
        super().__init__(interpreter)

    def get_message(self):
        return f"Słownik nie posiada atrybutu {self.name}"


class NonListIteratingError(InterpreterError):
    def get_message(self):
        return "Iterowanie na obiekcie niebędącym listą"


class ConditionNotBoolError(InterpreterError):
    def get_message(self):
        return "Warunek nie jest typu BOOL"


class DivisionByZeroError(InterpreterError):
    def get_message(self):
        return "Dzielenie przez zero"


class AssignmentVoidValue(InterpreterError):
    def get_message(self):
        return "Przypisanie do zmiennej wartości funkcji void"


class NotEnoughArgumentsError(InterpreterError):
    def __init__(self, interpreter, name):
        self.name = name
        super().__init__(interpreter)

    def get_message(self):
        return f"Za mało argumentów w wywołaniu funkcji {self.name}"


class BadArgumentTypeError(InterpreterError):
    def __init__(self, interpreter, name):
        self.name = name
        super().__init__(interpreter)

    def get_message(self):
        return f"Zły typ argumentu w wywołaniu funkcji {self.name}"
