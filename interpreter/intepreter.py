from enum import Enum

from interpreter.errors import OperationTypesError, InterpreterError, NoMainFunctionError, NoFunctionError, \
    VariableNotFoundError, IndexOutOfRangeError, IndexOnNonArrayError, AttributeOnNonDictError, \
    NonListIteratingError, ConditionNotBoolError, DivisionByZeroError, AssignmentVoidValue, NotEnoughArgumentsError, \
    BadArgumentTypeError, AttributeNotInDictError
from interpreter.functions import ElementFunction, AttributeFunction, AddChildFunction, AddAttributeFunction, Element, \
    Attribute, CopyFunction
from parser.objects import FunctionCall


class Scope:
    def __init__(self, init_variables=None):
        self.variables = {} if init_variables is None else init_variables

    def get_variable_by_name(self, name):
        return self.variables.get(name)

    def set_variable(self, name, value):
        self.variables[name] = value


class FunctionCallContext:
    def __init__(self, args, name):
        self.scopes = [Scope(args)]
        self.last_value = None
        self.name = name

    def get_variable(self, name):
        for scope in self.scopes[::-1]:
            value = scope.get_variable_by_name(name)
            if value is not None:
                return value

    def set_variable(self, name, value):
        variable = self.get_variable(name)
        if variable is None:
            self.scopes[-1].set_variable(name, value)
        else:
            variable.value = value.value
            variable.type = value.type

    def enter_scope(self):
        self.scopes.append(Scope())

    def leave_scope(self):
        self.scopes.pop()
        if len(self.scopes) == 0:
            print("DEBUG: 0 scopes!")


class Value:
    class Type(Enum):
        STRING = 0
        NUMBER = 1
        BOOL = 2
        LIST = 3
        DICT = 4
        ELEMENT = 5
        ATTRIBUTE = 6

    def __init__(self, type, value):
        self.type = type
        self.value = value

    def __str__(self):
        return str(self.value)

    def copy(self):
        if self.type.value <= Value.Type.BOOL.value:
            return Value(self.type, self.value)
        if self.type.value == Value.Type.LIST.value:
            return Value(self.type, [value.copy() for value in self.value])
        if self.type.value == Value.Type.DICT.value:
            return Value(Value.Type.DICT, {key: value.copy() for key, value in self.value.items()})
        return Value(self.type, self.value.copy())


class Interpreter:
    def __init__(self, program):
        self.last_result = None
        self.function_call_contexts = []
        self.program = program
        self.add_own_functions()
        self.return_executed = False

    def add_own_functions(self):
        self.program.functions.append(ElementFunction())
        self.program.functions.append(AttributeFunction())
        self.program.functions.append(AddChildFunction())
        self.program.functions.append(AddAttributeFunction())
        self.program.functions.append(CopyFunction())

    def visit_function_element(self):
        name = self.get_variable("name")
        if name.type.value != Value.Type.STRING.value:
            raise BadArgumentTypeError(self, "Element")
        element = Value(Value.Type.ELEMENT, Element(name))
        self.pop_last_function_call_context()
        return element

    def visit_function_attribute(self):
        name = self.get_variable("name")
        value = self.get_variable("value")
        if name.type.value != Value.Type.STRING.value or value.type.value not in [Value.Type.STRING.value,
                                                                                  Value.Type.NUMBER.value]:
            raise BadArgumentTypeError(self, "Attribute")

        element = Value(Value.Type.ATTRIBUTE,
                        Attribute(self.get_variable("name"), self.get_variable("value")))
        self.pop_last_function_call_context()
        return element

    def visit_function_add_attribute(self):
        element = self.get_variable("element")
        attribute = self.get_variable("attribute")
        if element.type.value != Value.Type.ELEMENT.value or attribute.type.value != Value.Type.ATTRIBUTE.value:
            raise BadArgumentTypeError(self, "addAttribute")
        element.value.attributes.append(self.get_variable("attribute"))
        self.pop_last_function_call_context()
        return element

    def visit_function_add_child(self):
        parent = self.get_variable("parent")
        child = self.get_variable("child")
        if parent.type.value != Value.Type.ELEMENT.value or (
                child.type.value not in [Value.Type.ELEMENT.value, Value.Type.STRING.value]):
            raise BadArgumentTypeError(self, "addChild")
        parent.value.children.append(child)
        self.pop_last_function_call_context()
        return parent

    def visit_function_copy(self):
        _obj = self.get_variable("object")
        self.pop_last_function_call_context()
        return _obj.copy()

    def accept_or_get_value(self, object_or_name):
        if type(object_or_name) == str:
            return self.get_variable(object_or_name)
        return object_or_name.accept(self)

    def get_function(self, name):
        if (function := self.program.get_function_by_name(name)) is None:
            raise NoFunctionError(self, name)
        return function

    def last_function_call_context(self):
        return self.function_call_contexts[-1]

    def enter_scope(self, init_variables=None):
        self.last_function_call_context().enter_scope()
        if init_variables:
            for name, value in init_variables.items():
                self.set_variable(name, value)

    def leave_scope(self):
        if not self.return_executed:
            self.last_function_call_context().leave_scope()

    def get_variable(self, name):
        if (variable := self.last_function_call_context().get_variable(name)) is None:
            raise VariableNotFoundError(self, name)
        return variable

    def set_variable(self, name, value):
        self.last_function_call_context().set_variable(name, value)

    def create_function_call_context(self, args, name):
        self.function_call_contexts.append(FunctionCallContext(args, name))

    def pop_last_function_call_context(self):
        self.function_call_contexts.pop()

    def visit_function_call(self, function_call):
        function_name = function_call.function
        function = self.get_function(function_name)
        if function.params is None:
            self.create_function_call_context({}, function_name)
        else:
            if (args_dict := function.params.create_args_dict(function_call.args)) is None:
                raise NotEnoughArgumentsError(self, function_name)
            self.create_function_call_context(args_dict.accept(self).value, function_name)
        return function.accept(self)

    def visit_function(self, function):
        result = function.block.accept(self)
        if not self.return_executed:
            self.pop_last_function_call_context()
        self.return_executed = False
        return result

    def visit_block(self, block):
        self.enter_scope()
        for statement in block.statements:
            result = statement.accept(self)

            if self.return_executed:
                self.leave_scope()
                return result

        self.leave_scope()

    def return_from_function(self, return_value):
        self.pop_last_function_call_context()
        self.return_executed = True
        return return_value

    def visit_constraint(self, constraint):
        return Value(constraint.type, constraint.value)

    def visit_list(self, _list):
        return Value(Value.Type.LIST, [value.accept(self) for value in _list.expressions])

    def visit_dict(self, _dict):
        new_dict = {}
        for key in _dict.dictionary:
            new_dict[key] = self.accept_or_get_value(_dict.dictionary[key])
        return Value(Value.Type.DICT, new_dict)

    def visit_assignment(self, assignment):
        condition_result = self.accept_or_get_value(assignment.condition)
        if condition_result.type.value != Value.Type.BOOL.value:
            raise ConditionNotBoolError(self)
        value = self.accept_or_get_value(assignment.value_if_true) \
            if condition_result.value \
            else self.accept_or_get_value(assignment.value_if_false)
        if value is None:
            raise AssignmentVoidValue(self)
        self.set_variable(assignment.name, value)

    def visit_return(self, return_statement):
        if return_statement.expression is None:
            return self.return_from_function(None)
        return self.return_from_function(self.accept_or_get_value(return_statement.expression))

    def visit_for(self, for_statement):
        iterator = self.accept_or_get_value(for_statement.expression).value
        if type(iterator) != list:
            raise NonListIteratingError(self)

        for i in iterator:
            self.enter_scope({for_statement.iterator_name: i})
            for_statement.block.accept(self)
            self.leave_scope()

    def visit_property(self, _property):
        dictionary = self.accept_or_get_value(_property.object).value
        if type(dictionary) != dict:
            raise AttributeOnNonDictError(self)

        if _property.name not in dictionary:
            raise AttributeNotInDictError(self, _property.name)

        return dictionary[_property.name]

    def visit_index(self, index):
        arr = self.accept_or_get_value(index.array).value
        index_value = index.index.value
        if type(arr) != list:
            raise IndexOnNonArrayError(self)
        if index_value >= len(arr):
            raise IndexOutOfRangeError(self, index_value)
        return arr[index_value]

    def visit_if(self, if_statement):
        for idx, condition in enumerate(if_statement.conditions):
            condition_value = self.accept_or_get_value(condition).value
            if type(condition_value) != bool:
                raise ConditionNotBoolError(self)
            if condition_value is True:
                result = if_statement.blocks[idx].accept(self)
                return result

        if if_statement.else_block is not None:
            result = if_statement.else_block.accept(self)
            return result

    def visit_program(self):
        return FunctionCall("Main", {}).accept(self)

    def operation_comparison(self, operation, _type):
        lfactor = self.accept_or_get_value(operation.lfactor)
        rfactor = self.accept_or_get_value(operation.rfactor)

        if lfactor.type.value != Value.Type.NUMBER.value or rfactor.type.value != Value.Type.NUMBER.value:
            raise OperationTypesError(self)

        match _type:
            case '<=':
                return Value(Value.Type.BOOL, lfactor.value <= rfactor.value)
            case '<':
                return Value(Value.Type.BOOL, lfactor.value < rfactor.value)
            case '>':
                return Value(Value.Type.BOOL, lfactor.value > rfactor.value)
            case _:
                return Value(Value.Type.BOOL, lfactor.value >= rfactor.value)

    def visit_operation_lte(self, operation):
        return self.operation_comparison(operation, '<=')

    def visit_operation_gte(self, operation):
        return self.operation_comparison(operation, '>=')

    def visit_operation_lt(self, operation):
        return self.operation_comparison(operation, '<')

    def visit_operation_gt(self, operation):
        return self.operation_comparison(operation, '>')

    def visit_operation_add(self, operation):
        lfactor = self.accept_or_get_value(operation.lfactor)
        rfactor = self.accept_or_get_value(operation.rfactor)

        if lfactor.type.value == rfactor.type.value:
            if lfactor.type.value == Value.Type.NUMBER.value:
                return Value(Value.Type.NUMBER, lfactor.value + rfactor.value)
            if lfactor.type.value == Value.Type.STRING.value:
                return Value(Value.Type.STRING, lfactor.value + rfactor.value)

        raise OperationTypesError(self)

    def visit_operation_sub(self, operation):
        lfactor = self.accept_or_get_value(operation.lfactor)
        rfactor = self.accept_or_get_value(operation.rfactor)

        if lfactor.type.value == rfactor.type.value:
            if lfactor.type.value == Value.Type.NUMBER.value:
                return Value(Value.Type.NUMBER, lfactor.value - rfactor.value)

        raise OperationTypesError(self)

    def visit_operation_mult(self, operation):
        lfactor = self.accept_or_get_value(operation.lfactor)
        rfactor = self.accept_or_get_value(operation.rfactor)

        if Value.Type.NUMBER.value == rfactor.type.value:
            if lfactor.type.value == Value.Type.NUMBER.value:
                return Value(Value.Type.NUMBER, lfactor.value * rfactor.value)
            if lfactor.type.value == Value.Type.STRING.value:
                return Value(Value.Type.STRING, lfactor.value * rfactor.value)
        raise OperationTypesError(self)

    def visit_negation(self, operation):
        factor = self.accept_or_get_value(operation.factor)

        if Value.Type.NUMBER.value == factor.type.value:
            return Value(Value.Type.NUMBER, -factor.value)
        if Value.Type.BOOL.value == factor.type.value:
            return Value(Value.Type.BOOL, not factor.value)

        raise OperationTypesError(self)

    def visit_operation_and(self, operation):
        lfactor = self.accept_or_get_value(operation.lfactor)
        if lfactor.type.value != Value.Type.BOOL.value:
            raise OperationTypesError(self)
        if lfactor.value is False:
            return Value(Value.Type.BOOL, False)

        rfactor = self.accept_or_get_value(operation.rfactor)
        if rfactor.type.value != Value.Type.BOOL.value:
            raise OperationTypesError(self)

        return Value(Value.Type.BOOL, rfactor.value)

    def visit_operation_or(self, operation):
        lfactor = self.accept_or_get_value(operation.lfactor)
        if lfactor.type.value != Value.Type.BOOL.value:
            raise OperationTypesError(self)
        if lfactor.value:
            return Value(Value.Type.BOOL, True)

        rfactor = self.accept_or_get_value(operation.rfactor)
        if rfactor.type.value != Value.Type.BOOL.value:
            raise OperationTypesError(self)

        return Value(Value.Type.BOOL, rfactor.value)

    def raise_division_by_zero_if_zero(self, value):
        if value == 0:
            raise DivisionByZeroError(self)

    def visit_operation_div(self, operation):
        lfactor = self.accept_or_get_value(operation.lfactor)
        rfactor = self.accept_or_get_value(operation.rfactor)

        if lfactor.type.value == rfactor.type.value:
            if lfactor.type.value == Value.Type.NUMBER.value:
                self.raise_division_by_zero_if_zero(rfactor.value)
                return Value(Value.Type.NUMBER, lfactor.value / rfactor.value)

        raise OperationTypesError(self)

    def visit_operation_div_int(self, operation):
        lfactor = self.accept_or_get_value(operation.lfactor)
        rfactor = self.accept_or_get_value(operation.rfactor)

        if lfactor.type.value == rfactor.type.value:
            if lfactor.type.value == Value.Type.NUMBER.value:
                self.raise_division_by_zero_if_zero(rfactor.value)
                return Value(Value.Type.NUMBER, lfactor.value // rfactor.value)

        raise OperationTypesError(self)

    def visit_operation_div_rest(self, operation):
        lfactor = self.accept_or_get_value(operation.lfactor)
        rfactor = self.accept_or_get_value(operation.rfactor)

        if lfactor.type.value == rfactor.type.value:
            if lfactor.type.value == Value.Type.NUMBER.value:
                self.raise_division_by_zero_if_zero(rfactor.value)
                return Value(Value.Type.NUMBER, lfactor.value % rfactor.value)

        raise OperationTypesError(self)

    def visit_operation_eq(self, operation):
        lfactor = self.accept_or_get_value(operation.lfactor)
        rfactor = self.accept_or_get_value(operation.rfactor)

        if lfactor.type.value == rfactor.type.value:
            if lfactor.type.value in [Value.Type.NUMBER.value, Value.Type.STRING.value]:
                return Value(Value.Type.BOOL, lfactor.value == rfactor.value)

        raise OperationTypesError(self)

    def visit_operation_not_eq(self, operation):
        lfactor = self.accept_or_get_value(operation.lfactor)
        rfactor = self.accept_or_get_value(operation.rfactor)

        if lfactor.type.value == rfactor.type.value:
            if lfactor.type.value in [Value.Type.NUMBER.value, Value.Type.STRING.value]:
                return Value(Value.Type.BOOL, lfactor.value != rfactor.value)

        raise OperationTypesError(self)

    def interpret(self):
        try:
            print(str(self.visit_program()))
        except InterpreterError as error:
            print(str(error))
        except Exception as e:
            print("Nieoczekiwany błąd", str(e))
