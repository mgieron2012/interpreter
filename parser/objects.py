import copy
from enum import Enum


class Function:
    def __init__(self, name, params, block):
        self.name = name
        self.params = params
        self.block = block

    def __eq__(self, other):
        return type(self) is type(other) \
            and self.name == other.name and self.params == other.params and self.block == other.block

    def accept(self, visitor):
        return visitor.visit_function(self)


class Params:
    def __init__(self, params):
        self.params = params

    def __eq__(self, other):
        return type(self) is type(other) and self.params == other.params

    def accept(self, visitor):
        return visitor.visit_function_params(self)

    def create_args_dict(self, list_of_args):
        result = copy.copy(self.params)
        i = 0
        for key in self.params:
            if i < len(list_of_args):
                result[key] = list_of_args[i]
                i += 1
        if all([value is not None for value in result.values()]):
            return Dictionary(result)


class Constraint:
    class Type(Enum):
        STRING = 0
        NUMBER = 1
        BOOL = 2

    def __init__(self, type, value):
        self.type = type
        self.value = value

    def __eq__(self, other):
        return self.type == other.type and self.value == other.value

    def accept(self, visitor):
        return visitor.visit_constraint(self)


class FunctionCall:
    def __init__(self, function, args):
        self.function = function
        self.args = args

    def __eq__(self, other):
        return type(self) is type(other) and self.function == other.function and self.args == other.args

    def accept(self, visitor):
        return visitor.visit_function_call(self)


class Negation:
    def __init__(self, factor) -> None:
        self.factor = factor

    def __eq__(self, other):
        return type(self) is type(other) and self.factor == other.factor

    def accept(self, visitor):
        return visitor.visit_negation(self)


class Index:
    def __init__(self, array, index):
        self.array = array
        self.index = index

    def __str__(self) -> str:
        return f"<Index value={self.index}>{self.array}</Index>"

    def __eq__(self, other):
        return type(self) is type(other) and self.array == other.array and self.index == other.index

    def accept(self, visitor):
        return visitor.visit_index(self)


class Property:
    def __init__(self, object, name):
        self.object = object
        self.name = name

    def __str__(self) -> str:
        return f"<Property name={self.name}>{self.object}</Property>"

    def __eq__(self, other):
        return type(self) is type(other) and self.object == other.object and self.name == other.name

    def accept(self, visitor):
        return visitor.visit_property(self)


class Operation:
    def __init__(self, lfactor, rfactor):
        self.lfactor = lfactor
        self.rfactor = rfactor

    def __eq__(self, other):
        return type(self) is type(other) and self.lfactor == other.lfactor and self.rfactor == other.rfactor


class OperationEQ(Operation):
    def accept(self, visitor):
        return visitor.visit_operation_eq(self)


class OperationGT(Operation):
    def accept(self, visitor):
        return visitor.visit_operation_gt(self)


class OperationGTE(Operation):
    def accept(self, visitor):
        return visitor.visit_operation_gte(self)


class OperationLT(Operation):
    def accept(self, visitor):
        return visitor.visit_operation_lt(self)


class OperationLTE(Operation):
    def accept(self, visitor):
        return visitor.visit_operation_lte(self)


class OperationNotEQ(Operation):
    def accept(self, visitor):
        return visitor.visit_operation_not_eq(self)


class OperationAnd(Operation):
    def accept(self, visitor):
        return visitor.visit_operation_and(self)


class OperationOr(Operation):
    def accept(self, visitor):
        return visitor.visit_operation_or(self)


class OperationAdd(Operation):
    def accept(self, visitor):
        return visitor.visit_operation_add(self)


class OperationSub(Operation):
    def accept(self, visitor):
        return visitor.visit_operation_sub(self)


class OperationMult(Operation):
    def accept(self, visitor):
        return visitor.visit_operation_mult(self)


class OperationDiv(Operation):
    def accept(self, visitor):
        return visitor.visit_operation_div(self)


class OperationDivInt(Operation):
    def accept(self, visitor):
        return visitor.visit_operation_div_int(self)


class OperationDivRest(Operation):
    def accept(self, visitor):
        return visitor.visit_operation_div_rest(self)


class Block:
    def __init__(self, statements) -> None:
        self.statements = statements

    def __str__(self) -> str:
        return f'<Block>{"".join(map(str, self.statements))}</Block>'

    def __eq__(self, other):
        return isinstance(other, Block) and self.statements == other.statements

    def accept(self, visitor):
        return visitor.visit_block(self)


class List:
    def __init__(self, expressions) -> None:
        self.expressions = expressions

    def __str__(self) -> str:
        return f'<List>{"".join(map(str, self.expressions))}</List>'

    def __eq__(self, other):
        return self.expressions == other.expressions

    def accept(self, visitor):
        return visitor.visit_list(self)


class Dictionary:
    def __init__(self, dictionary) -> None:
        self.dictionary = dictionary

    def __str__(self) -> str:
        return f'<Dictionary>{str(self.dictionary)}</Dictionary>'

    def __eq__(self, other):
        return self.dictionary == other.dictionary

    def accept(self, visitor):
        return visitor.visit_dict(self)


class ReturnStatement:
    def __init__(self, expression) -> None:
        self.expression = expression

    def __str__(self) -> str:
        return f'<Return>{str(self.expression)}</Return>'

    def __eq__(self, other):
        return self.expression == other.expression

    def accept(self, visitor):
        return visitor.visit_return(self)


class ForStatement:
    def __init__(self, iterator_name, expression, block) -> None:
        self.iterator_name = iterator_name
        self.expression = expression
        self.block = block

    def __str__(self) -> str:
        return f'<ForStatement iterator={self.iterator_name} in={str(self.expression)}>{str(self.block)}</ForStatement>'

    def __eq__(self, other):
        return self.iterator_name == other.iterator_name and \
            self.expression == other.expression and \
            self.block == other.block

    def accept(self, visitor):
        return visitor.visit_for(self)


class IfStatement:
    def __init__(self, conditions, blocks, else_block=None):
        self.conditions = conditions
        self.blocks = blocks
        self.else_block = else_block

    def __str__(self) -> str:
        return f'<IfStatement>{map(lambda cond, b: f"<If condition={str(cond)}>{str(b)}</If>", zip(self.conditions, self.blocks))}{f"<Else>{str(self.else_block)}</Else>" if self.blocks is not None else ""}</IfStatement>'

    def __eq__(self, other):
        return self.conditions == other.conditions and \
            self.blocks == other.blocks and \
            self.else_block == other.else_block

    def accept(self, visitor):
        return visitor.visit_if(self)


class Program:
    def __init__(self, functions) -> None:
        self.functions = functions

    def __str__(self) -> str:
        return f'<Program>{map(str, self.functions)}</Program>'

    def __eq__(self, other):
        return self.functions == other.functions

    def get_function_by_name(self, name):
        for function in self.functions:
            if function.name == name:
                return function

    def accept(self, visitor):
        return visitor.visit_program(self)


class Comment:
    def __init__(self, value) -> None:
        self.value = value

    def __str__(self) -> str:
        return f'<Comment>{self.value}</Comment>'

    def __eq__(self, other):
        return self.value == other.value


class Assignment:
    def __init__(self, name, value_if_true, condition=Constraint(Constraint.Type.BOOL, True), value_if_false=None):
        self.name = name
        self.value_if_true = value_if_true
        self.condition = condition
        self.value_if_false = value_if_false

    def __str__(self) -> str:
        if self.value_if_false is None:
            return f'<Assignment name={self.name}>{self.value_if_true}</Assignment>'
        return f'<Assignment name={self.name}>{self.value_if_true}<If>{self.condition}</If>{self.value_if_false}</Assignment>'

    def __eq__(self, other):
        return self.name == other.name and \
            self.value_if_false == other.value_if_false and \
            self.value_if_true == other.value_if_true and \
            self.condition == other.condition

    def accept(self, visitor):
        return visitor.visit_assignment(self)
