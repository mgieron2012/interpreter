from lexer.my_token import Token, TokenAddOperator, TokenMultOperator, TokenComparisonOperator
from parser.objects import Assignment, Block, Comment, Dictionary, ForStatement, Function, \
    IfStatement, List, Params, Constraint, FunctionCall, Index, \
    Program, Property, ReturnStatement, OperationAdd, OperationSub, Negation, OperationDivInt, \
    OperationDivRest, OperationEQ, OperationNotEQ, OperationGT, OperationLTE, OperationLT, OperationGTE, \
    OperationDiv, OperationAnd, OperationOr, OperationMult

# change try_parse_function_or_name


class Parser:
    def __init__(self, lexer) -> None:
        self.lexer = lexer
        self.mult_dict = {
            TokenMultOperator.Value.MULTIPLICATION: OperationMult,
            TokenMultOperator.Value.DIVISION: OperationDiv,
            TokenMultOperator.Value.INT_DIVISION: OperationDivInt,
            TokenMultOperator.Value.REST_DIVISION: OperationDivRest
        }
        self.comparison_dict = {
            TokenComparisonOperator.Value.EQUAL: OperationEQ,
            TokenComparisonOperator.Value.NOT_EQUAL: OperationNotEQ,
            TokenComparisonOperator.Value.GT: OperationGT,
            TokenComparisonOperator.Value.GT_EQ: OperationGTE,
            TokenComparisonOperator.Value.LT: OperationLT,
            TokenComparisonOperator.Value.LT_EQ: OperationLTE
        }

    def try_parse(self):
        functions = []

        while (function := self.try_parse_function_def()) is not None:
            functions.append(function)

        if self.token_type() != Token.Type.END:
            self.error("Unexpected token between function declarations")

        return Program(functions)

    def try_parse_function_def(self):
        # refactor consume and check type
        if self.token_type() != Token.Type.OP_DEF:
            return

        if self.next_token_type() != Token.Type.ID:
            self.error("Expected function name after def keyword")

        function_name = self.token_value()

        if self.next_token_type() != Token.Type.OP_PARENTHESIS_OPEN:
            self.error("No parenthesis after function name")
        self.get_next_token()

        params = self.try_parse_params()

        if self.token_type() != Token.Type.OP_PARENTHESIS_CLOSE:
            self.error("No closing parenthesis after function name")
        self.get_next_token()

        block = self.try_parse_block()
        if block is None:
            self.error("No function body")
        self.get_next_token()
        return Function(function_name, params, block)

    def try_parse_params(self):
        if self.token_type() != Token.Type.ID:
            return

        params = {}
        param_name = self.token_value()

        if self.next_token_type() == Token.Type.OP_ASSIGN:
            self.get_next_token()
            expression = self.try_parse_expression()
            if expression is None:
                self.error("No expression in assignment")
            params[param_name] = expression
        else:
            params[param_name] = None

        while self.token_type() == Token.Type.OP_COMMA:
            if self.next_token_type() != Token.Type.ID:
                self.error("Missing name in function arguments")

            param_name = self.token_value()

            if self.next_token_type() == Token.Type.OP_ASSIGN:
                self.get_next_token()
                expression = self.try_parse_expression()
                if expression is None:
                    self.error("Missing expression in function argument")
                if param_name in params:
                    self.error("Duplicated argument name")
                params[param_name] = expression
            else:
                if param_name in params:
                    self.error("Duplicated argument name")
                params[param_name] = None
        return Params(params)

    def try_parse_statement(self):
        if (function_def := self.try_parse_function_def()) is not None:
            return function_def

        if (return_statement := self.try_parse_return()) is not None:
            if self.token_type() != Token.Type.OP_SEMICOLON:
                self.error("Missing semicolon")
            self.get_next_token()
            return return_statement

        if (if_statement := self.try_parse_if()) is not None:
            return if_statement

        if (for_statement := self.try_parse_for()) is not None:
            return for_statement

        if (comment := self.try_parse_comment()) is not None:
            return comment

        if (function_call_or_name := self.try_parse_name_or_function_call()) is not None:
            if function_call_or_name[1]:
                statement = function_call_or_name[0]
            else:
                statement = self.try_parse_assignment(function_call_or_name[0])
                if statement is None:
                    self.error("Missing assignment is statement")
            if self.token_type() != Token.Type.OP_SEMICOLON:
                self.error("Missing semicolon")
            self.get_next_token()

            return statement

    def try_parse_expression(self):
        expression = self.try_parse_comparison()
        if expression is None:
            return

        while True:
            if self.token_type() == Token.Type.OP_AND:
                operation = OperationAnd
            elif self.token_type() == Token.Type.OP_OR:
                operation = OperationOr
            else:
                break
            self.get_next_token()
            comparison = self.try_parse_comparison()
            if comparison is None:
                self.error("Incorrect expression")
            expression = operation(expression, comparison)
        return expression

    def try_parse_comparison(self):
        comparison = self.try_parse_sum()

        if comparison is None:
            return

        if self.token_type() == Token.Type.OP_COMP:
            operation = self.comparison_dict.get(self.token_value())
            self.get_next_token()
            sum_ = self.try_parse_sum()

            if sum_ is None:
                self.error("Incorrect expression")

            return operation(comparison, sum_)
        return comparison

    def try_parse_sum(self):
        sum_ = self.try_parse_term()

        while self.token_type() == Token.Type.OP_ADD:
            operation = (OperationAdd
                         if self.token_value() == TokenAddOperator.Value.PLUS
                         else OperationSub)

            self.get_next_token()
            term = self.try_parse_term()
            if term is None:
                self.error("Incorrect term")

            sum_ = operation(sum_, term)

        return sum_

    def try_parse_term(self):
        term = self.try_parse_extended_factor()
        if term is None:
            return

        while self.token_type() == Token.Type.OP_MUL:
            operation = self.mult_dict.get(self.token_value())
            self.get_next_token()
            factor = self.try_parse_extended_factor()
            if factor is None:
                self.error("Incorrect factor")

            term = operation(term, factor)
        return term

    def try_parse_extended_factor(self):
        extended_factor = self.try_parse_unary_factor()
        if extended_factor is None:
            return

        while True:
            if self.token_type() == Token.Type.OP_DOT:
                self.get_next_token()
                if (name_or_function_call := self.try_parse_name_or_function_call()) is None:
                    self.error("Expected name or function call")
                if name_or_function_call[1]:
                    extended_factor = FunctionCall(
                        function=Property(extended_factor, name_or_function_call[0].function),
                        args=name_or_function_call[0].args)
                else:
                    extended_factor = Property(extended_factor, name_or_function_call[0])
            elif self.token_type() == Token.Type.OP_SQ_BR_OPEN:
                self.get_next_token()
                if (sum_ := self.try_parse_sum()) is None:
                    self.error("Incorrect expression")
                elif self.token_type() != Token.Type.OP_SQ_BR_CLOSE:
                    self.error("Missing closing brackets")
                else:
                    extended_factor = Index(extended_factor, sum_)
                    self.get_next_token()
            else:
                break
        return extended_factor

    def try_parse_unary_factor(self):
        if self.token_type() == Token.Type.OP_ADD:
            is_negative = self.token_value() == TokenAddOperator.Value.MINUS
            self.get_next_token()
            if (factor := self.try_parse_factor()) is None:
                self.error("Incorrect factor")
            else:
                return Negation(factor) if is_negative else factor
        return self.try_parse_factor()

    def try_parse_factor(self):
        if is_negation := (self.token_type() == Token.Type.OP_NOT):
            self.get_next_token()

        if self.token_type() == Token.Type.CONST:
            type = Constraint.Type(self.lexer.token.const_type.value)
            value = self.token_value()
            self.get_next_token()
            return Negation(Constraint(type, value)) if is_negation else Constraint(type, value)

        if self.token_type() == Token.Type.OP_PARENTHESIS_OPEN:
            self.get_next_token()
            expression = self.try_parse_expression()
            if self.token_type() != Token.Type.OP_PARENTHESIS_CLOSE:
                self.error("Missing closing parenthesis")
            self.get_next_token()
            return Negation(expression) if is_negation else expression

        if (list_ := self.try_parse_list()) is not None:
            return list_

        if (dict_ := self.try_parse_dictionary()) is not None:
            return dict_

        if (name_or_function_call := self.try_parse_name_or_function_call()) is not None:
            return Negation(name_or_function_call[0]) if is_negation else name_or_function_call[0]

    def try_parse_name_or_function_call(self):
        if self.token_type() != Token.Type.ID:
            return

        name = self.token_value()

        if self.next_token_type() != Token.Type.OP_PARENTHESIS_OPEN:
            return name, False
        args = []
        self.try_parse_dictionary()

        self.get_next_token()

        while (expression := self.try_parse_expression()) is not None:
            args.append(expression)
            if self.token_type() != Token.Type.OP_COMMA:
                break
            self.get_next_token()

        if self.token_type() != Token.Type.OP_PARENTHESIS_CLOSE:
            self.error("Missing closing parenthesis")

        self.get_next_token()
        return FunctionCall(name, args), True

    def try_parse_block(self):
        if self.token_type() != Token.Type.OP_CR_BR_OPEN:
            return

        statements = []
        self.get_next_token()
        while (statement := self.try_parse_statement()) is not None:
            statements.append(statement)

        if self.token_type() != Token.Type.OP_CR_BR_CLOSE:
            self.error("Missing closing brackets")
        self.get_next_token()

        return Block(statements)

    def try_parse_list(self):
        if self.token_type() != Token.Type.OP_SQ_BR_OPEN:
            return

        expressions = []

        self.get_next_token()
        expression = self.try_parse_expression()

        if expression is None:
            if self.token_type() != Token.Type.OP_SQ_BR_CLOSE:
                self.error("Missing closing brackets")
            self.get_next_token()
            return List(expressions)

        expressions.append(expression)

        while self.token_type() == Token.Type.OP_COMMA:
            self.get_next_token()
            expression = self.try_parse_expression()
            if expression is None:
                self.error("Incorrect expression")
            expressions.append(expression)

        if self.token_type() != Token.Type.OP_SQ_BR_CLOSE:
            self.error("Missing closing brackets")

        self.get_next_token()
        return List(expressions)

    def try_parse_dictionary(self):
        dictionary = {}
        if self.token_type() != Token.Type.OP_CR_BR_OPEN:
            return

        if self.next_token_type() != Token.Type.ID:
            if self.token_type() == Token.Type.OP_CR_BR_CLOSE:
                return Dictionary({})
            self.error("Missing closing brackets")

        name = self.token_value()
        if self.next_token_type() != Token.Type.OP_COLON:
            self.error("Missing colon")
        self.get_next_token()
        expression = self.try_parse_expression()

        if expression is None:
            self.error("Incorrect expression")

        dictionary[name] = expression

        while self.token_type() == Token.Type.OP_COMMA:
            if self.next_token_type() != Token.Type.ID:
                self.error("Missing name")
            name = self.token_value()
            if name in dictionary:
                self.error("Duplicated name in dictionary")

            if self.next_token_type() != Token.Type.OP_COLON:
                self.error("Missing colon")
            self.get_next_token()
            expression = self.try_parse_expression()
            if expression is None:
                self.error("Incorrect expression")
            dictionary[name] = expression

        if self.token_type() != Token.Type.OP_CR_BR_CLOSE:
            self.error("Missing closing brackets")
        self.get_next_token()

        return Dictionary(dictionary)

    def try_parse_return(self):
        if self.token_type() != Token.Type.OP_RETURN:
            return
        self.get_next_token()
        expression = self.try_parse_expression()
        if expression is None:
            self.error("Incorrect expression")
        return ReturnStatement(expression)

    def try_parse_for(self):
        if self.token_type() != Token.Type.OP_FOR:
            return
        if self.next_token_type() != Token.Type.ID:
            self.error("Missing name")

        iterator_name = self.token_value()

        if self.next_token_type() != Token.Type.OP_IN:
            self.error("Missing in operator")
        self.get_next_token()
        expression = self.try_parse_expression()
        if expression is None:
            self.error("Incorrect expression")
        block = self.try_parse_block()
        if block is None:
            self.error("Missing code block")
        return ForStatement(iterator_name, expression, block)

    def try_parse_if(self):
        if self.token_type() != Token.Type.OP_IF:
            return
        if self.next_token_type() != Token.Type.OP_PARENTHESIS_OPEN:
            self.error("Missing opening parenthesis")
        self.get_next_token()
        expression = self.try_parse_expression()

        if expression is None:
            self.error("Incorrect expression")
        if self.token_type() != Token.Type.OP_PARENTHESIS_CLOSE:
            self.error("Missing closing parenthesis")
        self.get_next_token()
        block = self.try_parse_block()
        if block is None:
            self.error("Missing code block")

        conditions = [expression]
        blocks = [block]

        while self.token_type() == Token.Type.OP_ELIF:
            if self.next_token_type() != Token.Type.OP_PARENTHESIS_OPEN:
                self.error("Missing opening parenthesis")
            self.get_next_token()
            expression = self.try_parse_expression()
            if expression is None:
                self.error("Incorrect expression")
            if self.token_type() != Token.Type.OP_PARENTHESIS_CLOSE:
                self.error("Missing opening parenthesis")
            self.get_next_token()
            block = self.try_parse_block()
            if block is None:
                self.error("Missing code block")
            conditions.append(expression)
            blocks.append(block)

        if self.token_type() == Token.Type.OP_ELSE:
            self.get_next_token()
            block = self.try_parse_block()
            if block is None:
                self.error("Missing code block")
            return IfStatement(conditions, blocks, block)

        return IfStatement(conditions, blocks)

    def try_parse_comment(self):
        if self.token_type() == Token.Type.COMMENT:
            value = self.token_value()
            self.get_next_token()
            return Comment(value)

    def try_parse_assignment(self, name):
        if self.token_type() != Token.Type.OP_ASSIGN:
            self.error("Missing assign operator")
        self.get_next_token()
        value = self.try_parse_expression()
        if value is None:
            self.error("Incorrect expression")
        if self.token_type() == Token.Type.OP_IF:
            self.get_next_token()
            condition = self.try_parse_expression()
            if condition is None:
                self.error("Missing condition")
            if self.token_type() != Token.Type.OP_ELSE:
                self.error("Missing else operator")
            self.get_next_token()
            else_value = self.try_parse_expression()
            if else_value is None:
                self.error("Missing else value")
            return Assignment(name, value, condition, else_value)
        return Assignment(name, value)

    def get_next_token(self, filter_comments=True):
        if not filter_comments:
            return self.lexer.get_next_token()

        while (token := self.lexer.get_next_token()).type == Token.Type.COMMENT:
            pass
        return token

    def next_token_type(self):
        return self.get_next_token().type

    def token_type(self):
        return self.lexer.token.type

    def token_value(self):
        return self.lexer.token.value

    def token_position(self):
        return self.lexer.token.position

    def error(self, message):
        print(f"({self.token_position().line}, {self.token_position().column}): {message}")


if __name__ == "__main__":
    import io
    from lexer.lexer import Lexer
    from lexer.source import Source

    p = Parser(Lexer(Source(io.StringIO(
        "15 + 17 * 22 >= 11 +- 5 and 13 - 12 <= 1"))))
    p.get_next_token()
    e = p.try_parse_expression()
    print(str(e))
