from lexer.my_token import Token, TokenConstraint, TokenAddOperator, TokenID, TokenComparisonOperator, TokenMultOperator
from parser.objects import OperationAnd, OperationGTE, OperationAdd, OperationLTE, OperationMult, Negation, \
    OperationSub, Function, Block, ReturnStatement, Params, List, Property, Index, Assignment
from parser.parser import Parser, Constraint


class Lexer:
    def __init__(self, tokens) -> None:
        self.tokens = tokens
        self.token = tokens[0]
        self.current = 0

    def get_next_token(self):
        self.current += 1
        if self.current < len(self.tokens):
            self.token = self.tokens[self.current]
        else:
            self.token = Token(None, Token.Type.END)
        return self.token


def test_parse_factor_number():
    p = Parser(
        Lexer([TokenConstraint(None, 315.4, TokenConstraint.ConstraintType.NUMBER)]))

    assert p.try_parse_factor() == Constraint(Constraint.Type.NUMBER, 315.4)


def test_parse_factor_string():
    p = Parser(
        Lexer([TokenConstraint(None, "ABC", TokenConstraint.ConstraintType.STRING)]))

    assert p.try_parse_factor() == Constraint(Constraint.Type.STRING, 'ABC')


def test_parse_factor_bool():
    p = Parser(
        Lexer([TokenConstraint(None, True, TokenConstraint.ConstraintType.BOOL)]))

    assert p.try_parse_factor() == Constraint(Constraint.Type.BOOL, True)


def test_parse_expression():
    # 15 + 17 * 22 >= 11 +- 5 and 13 - 12 <= 1
    p = Parser(Lexer([TokenConstraint(None, 15, TokenConstraint.ConstraintType.NUMBER),
                      TokenAddOperator(None, TokenAddOperator.Value.PLUS),
                      TokenConstraint(
                          None, 17, TokenConstraint.ConstraintType.NUMBER),
                      TokenMultOperator(None, TokenMultOperator.Value.MULTIPLICATION),
                      TokenConstraint(
                          None, 22, TokenConstraint.ConstraintType.NUMBER),
                      TokenComparisonOperator(None, TokenComparisonOperator.Value.GT_EQ),
                      TokenConstraint(
                          None, 11, TokenConstraint.ConstraintType.NUMBER),
                      TokenAddOperator(None, TokenAddOperator.Value.PLUS),
                      TokenAddOperator(None, TokenAddOperator.Value.MINUS),
                      TokenConstraint(
                          None, 5, TokenConstraint.ConstraintType.NUMBER),
                      Token(None, Token.Type.OP_AND),
                      TokenConstraint(
                          None, 13, TokenConstraint.ConstraintType.NUMBER),
                      TokenAddOperator(None, TokenAddOperator.Value.MINUS),
                      TokenConstraint(
                          None, 12, TokenConstraint.ConstraintType.NUMBER),
                      TokenComparisonOperator(None, TokenComparisonOperator.Value.LT_EQ),
                      TokenConstraint(
                          None, 1, TokenConstraint.ConstraintType.NUMBER),
                      Token(None, Token.Type.END)
                      ]))

    assert p.try_parse_expression() == OperationAnd(
        OperationGTE(
            OperationAdd(
                Constraint(Constraint.Type.NUMBER, 15),
                OperationMult(Constraint(Constraint.Type.NUMBER, 17), Constraint(Constraint.Type.NUMBER, 22))),
            OperationAdd(Constraint(Constraint.Type.NUMBER, 11), Negation(Constraint(Constraint.Type.NUMBER, 5)))),
        OperationLTE(OperationSub(Constraint(Constraint.Type.NUMBER, 13), Constraint(Constraint.Type.NUMBER, 12)),
                     Constraint(Constraint.Type.NUMBER, 1)))


def test_parse_function_def():
    # def foo(a, b=3){
    #   return a + b;
    # };
    p = Parser(Lexer([Token(None, Token.Type.OP_DEF),
                      TokenID(None, "foo"),
                      Token(None, Token.Type.OP_PARENTHESIS_OPEN),
                      Token(None, Token.Type.ID, "a"),
                      Token(None, Token.Type.OP_COMMA),
                      Token(None, Token.Type.ID, "b"),
                      Token(None, Token.Type.OP_ASSIGN),
                      TokenConstraint(
                          None, 3, TokenConstraint.ConstraintType.NUMBER),
                      Token(None, Token.Type.OP_PARENTHESIS_CLOSE),
                      Token(None, Token.Type.OP_CR_BR_OPEN),
                      Token(None, Token.Type.OP_RETURN),
                      TokenID(None, "a"),
                      TokenAddOperator(None, TokenAddOperator.Value.PLUS),
                      TokenID(None, "b"),
                      Token(None, Token.Type.OP_SEMICOLON),
                      Token(None, Token.Type.OP_CR_BR_CLOSE),
                      Token(None, Token.Type.END),
                      ]))

    assert p.try_parse_function_def() == Function("foo",
                                                  Params({'a': None, 'b': Constraint(Constraint.Type.NUMBER, 3)}),
                                                  Block([ReturnStatement(OperationAdd("a", "b"))]))


def test_parse_list():
    # l = [1, True, a * b, c[7].b.d];
    p = Parser(Lexer([TokenID(None, "l"),
                      Token(None, Token.Type.OP_ASSIGN),
                      Token(None, Token.Type.OP_SQ_BR_OPEN),
                      TokenConstraint(None, 1, TokenConstraint.ConstraintType.NUMBER),
                      Token(None, Token.Type.OP_COMMA),
                      TokenConstraint(None, True, TokenConstraint.ConstraintType.BOOL),
                      Token(None, Token.Type.OP_COMMA),
                      TokenID(None, "a"),
                      TokenMultOperator(None, TokenMultOperator.Value.MULTIPLICATION),
                      TokenID(None, "b"),
                      Token(None, Token.Type.OP_COMMA),
                      TokenID(None, "c"),
                      Token(None, Token.Type.OP_SQ_BR_OPEN),
                      TokenConstraint(None, 7, TokenConstraint.ConstraintType.NUMBER),
                      Token(None, Token.Type.OP_SQ_BR_CLOSE),
                      Token(None, Token.Type.OP_DOT),
                      TokenID(None, "b"),
                      Token(None, Token.Type.OP_DOT),
                      TokenID(None, "d"),
                      Token(None, Token.Type.OP_SQ_BR_CLOSE),
                      Token(None, Token.Type.OP_SEMICOLON),
                      Token(None, Token.Type.END),
                      ]))

    assert p.try_parse_statement() == Assignment("l", List(
        [Constraint(Constraint.Type.NUMBER, 1), Constraint(Constraint.Type.BOOL, True), OperationMult("a", "b"),
         Property(Property(Index("c", Constraint(Constraint.Type.NUMBER, 7)), "b"), "d")]))
