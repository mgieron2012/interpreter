from lexer.position import Position
from sys import float_info


class Error:
    def __init__(self):
        self.position = Position(1, 0)

    def set_position(self, position):
        self.position = position

    def __str__(self):
        return f'{str(self.position)}: ' + self.get_error_message()


class NoEndOfStringError(Error):
    def get_error_message(self):
        return "Nieoczekiwany koniec strumienia, oczekiwano: \" kończącego napis"
 

class NoEndOfCommentError(Error):
    def get_error_message(self):
        return "Nieoczekiwany koniec strumienia, oczekiwano: ; kończącego komentarz"


class UnknownTokenError(Error):
    def get_error_message(self):
        return "Nieprawidłowe wyrażenie"


class IDTooLongError(Error):
    def get_error_message(self):
        return "Przekroczono maksymalną długość identyfikatora."


class StringTooLongError(Error):
    def get_error_message(self):
        return "Przekroczono maksymalną długość napisu."


class CommentTooLongError(Error):
    def get_error_message(self):
        return "Przekroczono maksymalną długość komentarza."


class NumberTooBig(Error):
    def get_error_message(self):
        return f"Przekroczono maksymalną wartość liczby. Maksymalna wartość to: {float_info.max}"


class ExclamationMarkNotFollowedByEquals(Error):
    def get_error_message(self):
        return "Nieznana konstrukcja '!'. Traktuję jako '!='"


class EscapingError(Error):
    def get_error_message(self):
        return "Nieprawidłowy escaping, obsługiwane \\n, \\t, \\\\, \\\", \\;"


class TrailingZerosError(Error):
    def get_error_message(self):
        return "Nieprawidłowe zera przed liczbą"
