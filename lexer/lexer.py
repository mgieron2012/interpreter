from sys import float_info
from copy import copy
from lexer.my_token import Token, TokenConstraint, TokenID, TokenAddOperator, TokenComparisonOperator, TokenMultOperator
from lexer.error import EscapingError, ExclamationMarkNotFollowedByEquals, NoEndOfCommentError, NoEndOfStringError,\
    TrailingZerosError, UnknownTokenError, StringTooLongError, NumberTooBig, CommentTooLongError, IDTooLongError


class Lexer:
    def __init__(self, source, errors_limit=100, max_id_length=100, max_string_length=1000, max_comment_length=100,
                 error_handler=print):
        self.source = source
        self.buf = ''
        self.keywords = {
            'def': lambda: Token(self.token_start_position, Token.Type.OP_DEF),
            'if': lambda: Token(self.token_start_position, Token.Type.OP_IF),
            'elif': lambda: Token(self.token_start_position, Token.Type.OP_ELIF),
            'else': lambda: Token(self.token_start_position, Token.Type.OP_ELSE),
            'for': lambda: Token(self.token_start_position, Token.Type.OP_FOR),
            'in': lambda: Token(self.token_start_position, Token.Type.OP_IN),
            'return': lambda: Token(self.token_start_position, Token.Type.OP_RETURN),
            'not': lambda: Token(self.token_start_position, Token.Type.OP_NOT),
            'and': lambda: Token(self.token_start_position, Token.Type.OP_AND),
            'or': lambda: Token(self.token_start_position, Token.Type.OP_OR),
            'True': lambda: TokenConstraint(self.token_start_position, True, TokenConstraint.ConstraintType.BOOL),
            'False': lambda: TokenConstraint(self.token_start_position, False, TokenConstraint.ConstraintType.BOOL)
        }
        self.operators = {
            '+': lambda: TokenAddOperator(self.token_start_position, TokenAddOperator.Value.PLUS),
            '-': lambda: TokenAddOperator(self.token_start_position, TokenAddOperator.Value.MINUS),
            '[': lambda: Token(self.token_start_position, Token.Type.OP_SQ_BR_OPEN),
            ']': lambda: Token(self.token_start_position, Token.Type.OP_SQ_BR_CLOSE),
            '{': lambda: Token(self.token_start_position, Token.Type.OP_CR_BR_OPEN),
            '}': lambda: Token(self.token_start_position, Token.Type.OP_CR_BR_CLOSE),
            '(': lambda: Token(self.token_start_position, Token.Type.OP_PARENTHESIS_OPEN),
            ')': lambda: Token(self.token_start_position, Token.Type.OP_PARENTHESIS_CLOSE),
            '.': lambda: Token(self.token_start_position, Token.Type.OP_DOT),
            ';': lambda: Token(self.token_start_position, Token.Type.OP_SEMICOLON),
            ':': lambda: Token(self.token_start_position, Token.Type.OP_COLON),
            '=': lambda: Token(self.token_start_position, Token.Type.OP_ASSIGN),
            ',': lambda: Token(self.token_start_position, Token.Type.OP_COMMA),
            '==': lambda: TokenComparisonOperator(self.token_start_position, TokenComparisonOperator.Value.EQUAL),
            '!=': lambda: TokenComparisonOperator(self.token_start_position, TokenComparisonOperator.Value.NOT_EQUAL),
            '<': lambda: TokenComparisonOperator(self.token_start_position, TokenComparisonOperator.Value.LT),
            '<=': lambda: TokenComparisonOperator(self.token_start_position, TokenComparisonOperator.Value.LT_EQ),
            '>': lambda: TokenComparisonOperator(self.token_start_position, TokenComparisonOperator.Value.GT),
            '>=': lambda: TokenComparisonOperator(self.token_start_position, TokenComparisonOperator.Value.GT_EQ),
            '*': lambda: TokenMultOperator(self.token_start_position, TokenMultOperator.Value.MULTIPLICATION),
            '/': lambda: TokenMultOperator(self.token_start_position, TokenMultOperator.Value.DIVISION),
            '//': lambda: TokenMultOperator(self.token_start_position, TokenMultOperator.Value.INT_DIVISION),
            '%': lambda: TokenMultOperator(self.token_start_position, TokenMultOperator.Value.REST_DIVISION)
        }
        self.max_id_length = max_id_length
        self.max_string_length = max_string_length
        self.max_comment_length = max_comment_length
        self.errors = []
        self.errors_limit = errors_limit
        self._update_token_start_position()
        self.token = Token(self.token_start_position, Token.Type.UNKNOWN)
        self.error_handler = error_handler

    def get_next_token(self):
        if self._is_error_number_over_limit():
            self.token = Token(self.token_start_position, Token.Type.END)
            return self.token

        if self._is_buf_empty() or self._is_buf_whitespace():
            self._load_next_non_whitespace_char_to_buf()

        self._update_token_start_position()

        if not (self._build_end() or
                self._build_ID_or_keyword() or
                self._build_number() or
                self._build_string() or
                self._build_comment() or
                self._build_operator()):
            self._build_unknown()

        return self.token

    def _build_end(self):
        if self._is_buf_empty():
            self.token = Token(self.token_start_position, Token.Type.END)
            return True
            
    def _build_ID_or_keyword(self):
        if not self._is_buf_letter():
            return
        value = []
        while self._is_buf_letter() or self._is_buf_digit() or self.buf == "_":
            if len(value) == self.max_id_length:
                self._add_error(IDTooLongError(), False)
                while self._is_buf_letter() or self._is_buf_digit() or self.buf == "_":
                    self._load_next_char_to_buf()
                break

            value += self.buf
            self._load_next_char_to_buf()

        value = "".join(value)

        self.token = TokenID(self.token_start_position, value) if (
            token_lambda := self.keywords.get(value)) is None else token_lambda()
        return True

    def _build_number(self):   
        if not self._is_buf_digit():
            return 
        value = 0
        is_overflow = False

        if self.buf == '0':
            self._load_next_char_to_buf()
            if self._is_buf_digit():
                self._add_error(TrailingZerosError())
                self.token = Token(self.token_start_position, Token.Type.UNKNOWN) # or number <--
                return True
        else:
            while self._is_buf_digit():
                if is_overflow:
                    self._load_next_char_to_buf()
                    continue

                value *= 10
                value += self._get_digit_in_buf()

                # overflow can happen during int to float casting (after first '.' usage)
                if value > float_info.max:
                    is_overflow = True
                    value = float_info.max
                    self._add_error(NumberTooBig())

                self._load_next_char_to_buf()

        if not self.buf == ".":
            self.token = TokenConstraint(
            self.token_start_position, value, TokenConstraint.ConstraintType.NUMBER)
            return True

        value_after_point = 0
        len_after_point = 0
        self._load_next_char_to_buf()

        while self._is_buf_digit():
            value_after_point *= 10
            value_after_point += self._get_digit_in_buf()
            len_after_point += 1

            self._load_next_char_to_buf()

        if not is_overflow:
            self.token = TokenConstraint(self.token_start_position,
                                        value + value_after_point / 10 ** len_after_point, TokenConstraint.ConstraintType.NUMBER)
            return True

    def _build_string(self):
        if self.buf != "\"":
            return
        value = []
        self._load_next_char_to_buf()

        while self.buf != "\"":
            if self._is_buf_empty():
                self._add_error(NoEndOfStringError())
                break

            self._perform_escaping()

            if len(value) == self.max_string_length:
                self._add_error(StringTooLongError(), False)
                while not (self.buf == "\"" or self._is_buf_empty()):
                    self._perform_escaping()
                    self._load_next_char_to_buf()
                break

            value += self.buf
            self._load_next_char_to_buf()

        value = "".join(value)

        # last loaded char is used, load next char
        self._load_next_char_to_buf()
        self.token = TokenConstraint(
            self.token_start_position, value, TokenConstraint.ConstraintType.STRING)
        return True

    def _build_comment(self):
        if self.buf == "#":
            value = []
            self._load_next_char_to_buf()

            while self.buf != ";":
                if len(value) == self.max_comment_length:
                    self._add_error(CommentTooLongError(), False)
                    while not (self.buf == ";" or self._is_buf_empty()):
                        self._load_next_char_to_buf()
                    break

                value += self.buf
                self._load_next_char_to_buf()

                if self._is_buf_empty():
                    self._add_error(NoEndOfCommentError())
                    break
            value = "".join(value)

            # last loaded char is used, load next char
            self._load_next_non_whitespace_char_to_buf()
            self.token = Token(self.token_start_position,
                               Token.Type.COMMENT, value)
            return True

    def _build_operator(self):
        if self.buf == '!':
            self._load_next_char_to_buf()
            if self.buf == '=':
                self._load_next_char_to_buf()
            else:
                self._add_error(ExclamationMarkNotFollowedByEquals())

            self.token = self.operators.get('!=')()
            return True

        if (token_lambda := self.operators.get(self.buf)) is not None:
            value = self.buf
            self._load_next_char_to_buf()
            value += self.buf

            if (double_char_token_lambda := self.operators.get(value)) is not None:
                self._load_next_char_to_buf()
                self.token = double_char_token_lambda()
            else:
                self.token = token_lambda()
            return True

    def _build_unknown(self):
        self._add_error(UnknownTokenError())
        self.token = Token(self.token_start_position, Token.Type.UNKNOWN)

    def _load_next_non_whitespace_char_to_buf(self):
        self._load_next_char_to_buf()
        while self._is_buf_whitespace():
            self._load_next_char_to_buf()

    def _load_next_char_to_buf(self):
        self.buf = self.source.get_next()

    def _is_buf_empty(self):
        return self.buf == ''

    def _is_buf_whitespace(self):
        return self.buf.isspace()

    def _is_buf_letter(self):
        return self.buf.isalpha()

    def _is_buf_digit(self):
        return self.buf.isdecimal()
    
    def _is_buf_non_zero_digit(self):
        return self.buf.isdecimal() and self.buf != '0'

    def _add_error(self, error, set_end_of_token_position=True):
        # funkcja zewnetrzna
        if set_end_of_token_position:
            error.set_position(self._get_position())
        else:
            error.set_position(self.token_start_position)

        self.error_handler(error)
        self.errors.append(error)

    def _is_error_number_over_limit(self):
        return len(self.errors) > self.errors_limit

    def _get_digit_in_buf(self):
        return ord(self.buf) - ord('0')

    def _get_position(self):
        return self.source.get_position()

    def _update_token_start_position(self):
        self.token_start_position = copy(self._get_position())

    def _perform_escaping(self):
        if self.buf == '\\':
            self._load_next_char_to_buf()
            if self.buf == 't':
                self.buf = '\t'
            elif self.buf == 'n':
                self.buf = '\n'
            elif self.buf == '\\':
                self.buf = '\\'
            elif self.buf != '"':
                self._add_error(EscapingError())
