from lexer.position import Position


class Source:
    def __init__(self, reader):
        self.position = Position(1, 0)
        self.reader = reader
        self.line_offsets = [reader.tell()]

    def get_position(self):
        return self.position
    
    def _update_state_after_get_next(self, char):     
        # TextIOWrapper replace \r with \n
        if char == '\n':
            self.position.move_line()
            self.line_offsets.append(self.reader.tell())
        else:
            self.position.move()

    def get_next(self):
        # reads 1 character
        char = self.reader.read(1)
        if char:
            self._update_state_after_get_next(char)
        return char
    
    def get_line(self, line):
        if 1 <= line <= len(self.line_offsets):
            self.reader.seek(self.line_offsets[line-1])
            # odtworzyc pozycje sprzed get_line
            return self.reader.readline().strip()
