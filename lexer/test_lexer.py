import pytest
import io
from sys import float_info

from lexer.source import Source
from lexer.lexer import Lexer
from lexer.my_token import Token, TokenConstraint, TokenAddOperator, TokenComparisonOperator, TokenMultOperator


class TestLexer:
    def test_identifier(self):
        token = Lexer(Source(io.StringIO("uwu"))).get_next_token()

        assert token.type == Token.Type.ID
        assert token.value == "uwu"
        assert token.position.line == 1
        assert token.position.column == 1

    def test_identifier_single_letter(self):
        token = Lexer(Source(io.StringIO("  a"))).get_next_token()

        assert token.type == Token.Type.ID
        assert token.value == "a"
        assert token.position.line == 1
        assert token.position.column == 3

    def test_identifier_single_letter(self):
        token = Lexer(Source(io.StringIO("屋"))).get_next_token()

        assert token.type == Token.Type.ID
        assert token.value == "屋"
        assert token.position.line == 1
        assert token.position.column == 1

    def test_identifier_with_underscore(self):
        token = Lexer(Source(io.StringIO("U___U"))).get_next_token()

        assert token.type == Token.Type.ID
        assert token.value == "U___U"
        assert token.position.line == 1
        assert token.position.column == 1

    def test_identifier_with_number(self):
        token = Lexer(Source(io.StringIO(" a12_cc_1 "))).get_next_token()

        assert token.type == Token.Type.ID
        assert token.value == "a12_cc_1"
        assert token.position.line == 1
        assert token.position.column == 2

    def test_keyword_if(self):
        token = Lexer(Source(io.StringIO(" if "))).get_next_token()

        assert token.type == Token.Type.OP_IF
        assert token.position.line == 1
        assert token.position.column == 2

    def test_keyword_def(self):
        token = Lexer(Source(io.StringIO(" def "))).get_next_token()

        assert token.type == Token.Type.OP_DEF
        assert token.position.line == 1
        assert token.position.column == 2

    def test_keyword_elif(self):
        token = Lexer(Source(io.StringIO(" elif "))).get_next_token()

        assert token.type == Token.Type.OP_ELIF
        assert token.position.line == 1
        assert token.position.column == 2

    def test_keyword_else(self):
        token = Lexer(Source(io.StringIO(" else "))).get_next_token()

        assert token.type == Token.Type.OP_ELSE
        assert token.position.line == 1
        assert token.position.column == 2

    def test_keyword_for(self):
        token = Lexer(Source(io.StringIO(" for "))).get_next_token()

        assert token.type == Token.Type.OP_FOR
        assert token.position.line == 1
        assert token.position.column == 2

    def test_keyword_in(self):
        token = Lexer(Source(io.StringIO(" in "))).get_next_token()

        assert token.type == Token.Type.OP_IN
        assert token.position.line == 1
        assert token.position.column == 2

    def test_keyword_return(self):
        token = Lexer(Source(io.StringIO(" return "))).get_next_token()

        assert token.type == Token.Type.OP_RETURN
        assert token.position.line == 1
        assert token.position.column == 2

    def test_keyword_not(self):
        token = Lexer(Source(io.StringIO(" not "))).get_next_token()

        assert token.type == Token.Type.OP_NOT
        assert token.position.line == 1
        assert token.position.column == 2

    def test_keyword_and(self):
        token = Lexer(Source(io.StringIO(" and "))).get_next_token()

        assert token.type == Token.Type.OP_AND
        assert token.position.line == 1
        assert token.position.column == 2

    def test_keyword_or(self):
        token = Lexer(Source(io.StringIO(" or "))).get_next_token()

        assert token.type == Token.Type.OP_OR
        assert token.position.line == 1
        assert token.position.column == 2

    def test_number(self):
        token = Lexer(Source(io.StringIO(" 156 "))).get_next_token()

        assert token.type == Token.Type.CONST
        assert token.value == 156
        assert token.const_type == TokenConstraint.ConstraintType.NUMBER
        assert token.position.line == 1
        assert token.position.column == 2

    def test_number_float(self):
        token = Lexer(Source(io.StringIO(" 156.13 "))).get_next_token()

        assert token.type == Token.Type.CONST
        assert token.value == pytest.approx(156.13)
        assert token.const_type == TokenConstraint.ConstraintType.NUMBER
        assert token.position.line == 1
        assert token.position.column == 2

    def test_number_two_dots(self):
        token = Lexer(Source(io.StringIO(" 156.13.11 "))).get_next_token()

        assert token.type == Token.Type.CONST
        assert token.value == pytest.approx(156.13)
        assert token.const_type == TokenConstraint.ConstraintType.NUMBER
        assert token.position.line == 1
        assert token.position.column == 2

    def test_number_too_big(self):
        lexer = Lexer(Source(io.StringIO(str(int(float_info.max) + 1))))
        token = lexer.get_next_token()

        assert token.type == Token.Type.CONST
        assert token.value == pytest.approx(float_info.max)
        assert token.const_type == TokenConstraint.ConstraintType.NUMBER
        assert token.position.line == 1
        assert token.position.column == 1
        assert len(lexer.errors) == 1
        assert str(lexer.errors[0]) == f"(1, 309): Przekroczono maksymalną wartość liczby. Maksymalna wartość to: {float_info.max}"


    def test_string(self):
        token = Lexer(Source(io.StringIO(" \" test \" "))).get_next_token()

        assert token.type == Token.Type.CONST
        assert token.value == " test "
        assert token.const_type == TokenConstraint.ConstraintType.STRING
        assert token.position.line == 1
        assert token.position.column == 2

    def test_string_with_spec_chars(self):
        token = Lexer(Source(io.StringIO(" \" test123;'?.\" "))).get_next_token()

        assert token.type == Token.Type.CONST
        assert token.value == " test123;'?."
        assert token.const_type == TokenConstraint.ConstraintType.STRING
        assert token.position.line == 1
        assert token.position.column == 2

    def test_string_no_end(self):
        lexer = Lexer(Source(io.StringIO(" \" test123;'?.")))
        token = lexer.get_next_token()

        assert token.type == Token.Type.CONST
        assert token.value == " test123;'?."
        assert token.const_type == TokenConstraint.ConstraintType.STRING
        assert token.position.line == 1
        assert token.position.column == 2
        assert str(lexer.errors[0]) == "(1, 14): Nieoczekiwany koniec strumienia, oczekiwano: \" kończącego napis"

    def test_comment(self):
        token = Lexer(Source(io.StringIO(" # test 123; "))).get_next_token()

        assert token.type == Token.Type.COMMENT
        assert token.value == " test 123"
        assert token.position.line == 1
        assert token.position.column == 2

    def test_comment_no_end(self):
        lexer = Lexer(Source(io.StringIO(" # test 123 ")))
        token = lexer.get_next_token()

        assert token.type == Token.Type.COMMENT
        assert token.value == " test 123 "
        assert token.position.line == 1
        assert token.position.column == 2
        assert str(lexer.errors[0]) == "(1, 12): Nieoczekiwany koniec strumienia, oczekiwano: ; kończącego komentarz"

    def test_plus(self):
        token = Lexer(Source(io.StringIO("+1"))).get_next_token()

        assert token.type == Token.Type.OP_ADD
        assert token.value == TokenAddOperator.Value.PLUS
        assert token.position.line == 1
        assert token.position.column == 1

    def test_minus(self):
        token = Lexer(Source(io.StringIO("-1"))).get_next_token()

        assert token.type == Token.Type.OP_ADD
        assert token.value == TokenAddOperator.Value.MINUS
        assert token.position.line == 1
        assert token.position.column == 1

    def test_muls(self):
        lexer = Lexer(Source(io.StringIO("*/%//r")))
        tokens = [lexer.get_next_token() for i in range(4)]

        assert tokens[0].type == Token.Type.OP_MUL
        assert tokens[0].value == TokenMultOperator.Value.MULTIPLICATION
        assert tokens[1].type == Token.Type.OP_MUL
        assert tokens[1].value == TokenMultOperator.Value.DIVISION
        assert tokens[2].type == Token.Type.OP_MUL
        assert tokens[2].value == TokenMultOperator.Value.REST_DIVISION
        assert tokens[3].type == Token.Type.OP_MUL
        assert tokens[3].value == TokenMultOperator.Value.INT_DIVISION

    def test_error_NOT_EQ(self):
        lexer = Lexer(Source(io.StringIO("!a")))
        assert lexer.get_next_token().value == TokenComparisonOperator.Value.NOT_EQUAL
        assert len(lexer.errors) == 1

    def test_comparisons(self):
        lexer = Lexer(Source(io.StringIO("< <= > >= != == =")))
        tokens = [lexer.get_next_token() for i in range(8)]

        assert tokens[0].type == Token.Type.OP_COMP
        assert tokens[0].value == TokenComparisonOperator.Value.LT
        assert tokens[1].type == Token.Type.OP_COMP
        assert tokens[1].value == TokenComparisonOperator.Value.LT_EQ
        assert tokens[2].type == Token.Type.OP_COMP
        assert tokens[2].value == TokenComparisonOperator.Value.GT
        assert tokens[3].type == Token.Type.OP_COMP
        assert tokens[3].value == TokenComparisonOperator.Value.GT_EQ
        assert tokens[4].type == Token.Type.OP_COMP
        assert tokens[4].value == TokenComparisonOperator.Value.NOT_EQUAL
        assert tokens[5].type == Token.Type.OP_COMP
        assert tokens[5].value == TokenComparisonOperator.Value.EQUAL
        assert tokens[6].type == Token.Type.OP_ASSIGN
        assert tokens[7].type == Token.Type.END

    def test_other_operators(self):
        lexer = Lexer(Source(io.StringIO("[]{}().;:=")))
        tokens = [lexer.get_next_token() for i in range(10)]

        assert tokens[0].type == Token.Type.OP_SQ_BR_OPEN
        assert tokens[1].type == Token.Type.OP_SQ_BR_CLOSE
        assert tokens[2].type == Token.Type.OP_CR_BR_OPEN
        assert tokens[3].type == Token.Type.OP_CR_BR_CLOSE
        assert tokens[4].type == Token.Type.OP_PARENTHESIS_OPEN
        assert tokens[5].type == Token.Type.OP_PARENTHESIS_CLOSE
        assert tokens[6].type == Token.Type.OP_DOT
        assert tokens[7].type == Token.Type.OP_SEMICOLON
        assert tokens[8].type == Token.Type.OP_COLON
        assert tokens[9].type == Token.Type.OP_ASSIGN

    def test_string_length_limit(self):
        lexer = Lexer(Source(io.StringIO("\"abcd\"")), max_string_length=1)

        assert lexer.get_next_token().value == "a"
        assert lexer.get_next_token().type == Token.Type.END
        assert len(lexer.errors) == 1
        assert str(lexer.errors[0]) == "(1, 1): Przekroczono maksymalną długość napisu."

    def test_comment_length_limit(self):
        lexer = Lexer(Source(io.StringIO("# abcd;")), max_comment_length=3)

        assert lexer.get_next_token().value == " ab"
        assert lexer.get_next_token().type == Token.Type.END
        assert len(lexer.errors) == 1
        assert str(lexer.errors[0]) == "(1, 1): Przekroczono maksymalną długość komentarza."

    def test_ID_length_limit(self):
        lexer = Lexer(Source(io.StringIO("ab")), max_id_length=1)

        assert lexer.get_next_token().value == "a"
        assert lexer.get_next_token().type == Token.Type.END
        assert len(lexer.errors) == 1
        assert str(lexer.errors[0]) == "(1, 1): Przekroczono maksymalną długość identyfikatora."

    def test_if(self):
        code = """
    if ( a < 10 ){
        return 10.1;
    } else {
        return "A";
    }
        """
        lexer = Lexer(Source(io.StringIO(code)))

        assert lexer.get_next_token().type == Token.Type.OP_IF
        assert lexer.get_next_token().type == Token.Type.OP_PARENTHESIS_OPEN
        assert lexer.get_next_token().value == 'a'
        assert lexer.get_next_token().value == TokenComparisonOperator.Value.LT
        assert lexer.get_next_token().value == 10
        assert lexer.get_next_token().type == Token.Type.OP_PARENTHESIS_CLOSE
        assert lexer.get_next_token().type == Token.Type.OP_CR_BR_OPEN
        assert lexer.get_next_token().type == Token.Type.OP_RETURN
        assert lexer.get_next_token().value == pytest.approx(10.1)
        assert lexer.get_next_token().type == Token.Type.OP_SEMICOLON
        assert lexer.get_next_token().type == Token.Type.OP_CR_BR_CLOSE
        assert lexer.get_next_token().type == Token.Type.OP_ELSE
        assert lexer.get_next_token().type == Token.Type.OP_CR_BR_OPEN
        assert lexer.get_next_token().type == Token.Type.OP_RETURN
        token = lexer.get_next_token()
        assert token.value == "A"
        assert token.const_type == TokenConstraint.ConstraintType.STRING
        assert token.position.line == 5
        assert token.position.column == 16
        assert lexer.get_next_token().type == Token.Type.OP_SEMICOLON
        assert lexer.get_next_token().type == Token.Type.OP_CR_BR_CLOSE
        assert lexer.get_next_token().type == Token.Type.END

    def test_source_getline(self):
        code="""
        if ( a < 10 ){
            return 10.1;
        } else {
            return "屋";
        }
        """
        source = Source(io.StringIO(code))
        lexer = Lexer(source)

        while lexer.get_next_token().type != Token.Type.END:
            pass

        assert source.get_line(1) == ""
        assert source.get_line(2) == "if ( a < 10 ){"
        assert source.get_line(3) == "return 10.1;"
        assert source.get_line(4) == "} else {"
        assert source.get_line(5) == "return \"屋\";"
        assert source.get_line(6) == "}"
        assert source.get_line(7) == ""

    def test_file_source(self):
        with open("./test.txt", "r") as file:
            lexer = Lexer(Source(file))

            assert lexer.get_next_token().value == '屋'
            assert lexer.get_next_token().type == Token.Type.OP_IF
            assert lexer.get_next_token().type == Token.Type.OP_PARENTHESIS_OPEN
            assert lexer.get_next_token().value == 'a'
            assert lexer.get_next_token().value == TokenComparisonOperator.Value.LT
            assert lexer.get_next_token().value == 10
            assert lexer.get_next_token().type == Token.Type.OP_PARENTHESIS_CLOSE
            assert lexer.get_next_token().type == Token.Type.OP_CR_BR_OPEN
            assert lexer.get_next_token().type == Token.Type.OP_RETURN
            assert lexer.get_next_token().value == pytest.approx(10.1)
            assert lexer.get_next_token().type == Token.Type.OP_SEMICOLON
            assert lexer.get_next_token().type == Token.Type.OP_CR_BR_CLOSE
            assert lexer.get_next_token().type == Token.Type.OP_ELSE
            assert lexer.get_next_token().type == Token.Type.OP_CR_BR_OPEN
            assert lexer.get_next_token().type == Token.Type.OP_RETURN
            token = lexer.get_next_token()
            assert token.value == "A"
            assert token.const_type == TokenConstraint.ConstraintType.STRING
            assert token.position.line == 5
            assert token.position.column == 12
            assert lexer.get_next_token().type == Token.Type.OP_SEMICOLON
            assert lexer.get_next_token().type == Token.Type.OP_CR_BR_CLOSE
            assert lexer.get_next_token().type == Token.Type.END
            assert lexer.source.get_line(3) == "return 10.1;"

    def test_dict(self):
        code="""
        def DictExample(){
            x = {
                a: 5,
                b: "b",
                c: True
            };
            return x.a;
        }
        """
        lexer = Lexer(Source(io.StringIO(code)))

        assert lexer.get_next_token().type == Token.Type.OP_DEF
        assert lexer.get_next_token().value == 'DictExample'
        assert lexer.get_next_token().type == Token.Type.OP_PARENTHESIS_OPEN
        assert lexer.get_next_token().type == Token.Type.OP_PARENTHESIS_CLOSE
        assert lexer.get_next_token().type == Token.Type.OP_CR_BR_OPEN
        assert lexer.get_next_token().value == 'x'
        assert lexer.get_next_token().type == Token.Type.OP_ASSIGN
        assert lexer.get_next_token().type == Token.Type.OP_CR_BR_OPEN
        assert lexer.get_next_token().value == 'a'
        assert lexer.get_next_token().type == Token.Type.OP_COLON
        assert lexer.get_next_token().value == 5
        assert lexer.get_next_token().type == Token.Type.OP_COMMA
        assert lexer.get_next_token().value == 'b'
        assert lexer.get_next_token().type == Token.Type.OP_COLON
        assert lexer.get_next_token().value == "b"
        assert lexer.get_next_token().type == Token.Type.OP_COMMA
        assert lexer.get_next_token().value == 'c'
        assert lexer.get_next_token().type == Token.Type.OP_COLON
        assert lexer.get_next_token().value == True
        assert lexer.get_next_token().type == Token.Type.OP_CR_BR_CLOSE
        assert lexer.get_next_token().type == Token.Type.OP_SEMICOLON
        assert lexer.get_next_token().type == Token.Type.OP_RETURN
        assert lexer.get_next_token().value == 'x'
        assert lexer.get_next_token().type == Token.Type.OP_DOT
        assert lexer.get_next_token().value == 'a'
        assert lexer.get_next_token().type == Token.Type.OP_SEMICOLON
        assert lexer.get_next_token().type == Token.Type.OP_CR_BR_CLOSE
        assert lexer.get_next_token().type == Token.Type.END

    def test_unknown(self):
        lexer = Lexer(Source(io.StringIO("a^")))

        assert lexer.get_next_token().value == 'a'
        assert lexer.get_next_token().type == Token.Type.UNKNOWN
        assert str(lexer.errors[0]) == "(1, 2): Nieprawidłowe wyrażenie"

    def test_whitespaces(self):
        lexer = Lexer(Source(io.StringIO(" " * 1005)))

        assert lexer.get_next_token().type == Token.Type.END
        assert len(lexer.errors) == 0

    def test_exclamation_mark(self):
        lexer = Lexer(Source(io.StringIO("!")))

        assert lexer.get_next_token().value == TokenComparisonOperator.Value.NOT_EQUAL
        assert lexer.get_next_token().type == Token.Type.END
        assert str(lexer.errors[0]) == "(1, 1): Nieznana konstrukcja '!'. Traktuję jako '!='"

    def test_escaping_string(self):
        lexer = Lexer(Source(io.StringIO(f'"\\\\a\\\\"')))

        assert lexer.get_next_token().value == "\\a\\"

    def test_escaping(self):
        lexer = Lexer(Source(io.StringIO(f'"\\""')))

        assert lexer.get_next_token().value == "\""

    def test_escaping_tab(self):
        lexer = Lexer(Source(io.StringIO(f'"\\t"')))

        assert lexer.get_next_token().value == "\t"

    def test_escaping_newline(self):
        lexer = Lexer(Source(io.StringIO(f'"\\n"')))

        assert lexer.get_next_token().value == "\n"

    def test_escaping_semicolon(self):
        lexer = Lexer(Source(io.StringIO(f'"\\;"')))

        assert lexer.get_next_token().value == ";"

    def test_escaping_incorrect(self):
        lexer = Lexer(Source(io.StringIO(f'"\\r"')))

        assert lexer.get_next_token().value == "r"
        assert str(lexer.errors[0]) == "(1, 4): Nieprawidłowy escaping, obsługiwane \\n, \\t, \\\\, \\\", \\;"

    def test_escaping_comment(self):
        lexer = Lexer(Source(io.StringIO("# \\\\comment\\; 1;")))

        assert lexer.get_next_token().value == " \\\\comment\\"

    def test_number_007(self):
        lexer = Lexer(Source(io.StringIO("007")))

        assert lexer.get_next_token().type == Token.Type.UNKNOWN
        assert lexer.get_next_token().type == Token.Type.UNKNOWN
        assert lexer.get_next_token().value == 7
        assert len(lexer.errors) == 2
        assert str(lexer.errors[0]) == "(1, 3): Nieprawidłowe zera przed liczbą"

    def test_number_0_point_07(self):
        lexer = Lexer(Source(io.StringIO("0.07")))

        assert lexer.get_next_token().value == pytest.approx(0.07)
        assert len(lexer.errors) == 0
