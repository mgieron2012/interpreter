class Position:
    def __init__(self, line, column):
        self.line = line
        self.column = column
    
    def __str__(self) -> str:
        return f'({self.line}, {self.column})'

    def move(self):
        self.column += 1
    
    def move_line(self):
        self.column = 0
        self.line += 1
