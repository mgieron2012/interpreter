from enum import Enum


class Token:
    class Type(Enum):
        ID = 0
        CONST = 1
        END = 2
        OP_ADD = 3
        OP_MUL = 4
        OP_COMP = 5
        COMMENT = 6
        OP_DEF = 7
        OP_IF = 8
        OP_ELIF = 9
        OP_ELSE = 10
        OP_FOR = 11
        OP_IN = 12
        OP_RETURN = 13
        OP_NOT = 14
        OP_AND = 15
        OP_OR = 16
        OP_SQ_BR_OPEN = 17
        OP_SQ_BR_CLOSE = 18
        OP_CR_BR_OPEN = 19
        OP_CR_BR_CLOSE = 20
        OP_PARENTHESIS_OPEN = 21
        OP_PARENTHESIS_CLOSE = 22
        OP_DOT = 23
        OP_SEMICOLON = 24
        OP_COMMA = 25
        OP_ASSIGN = 26
        OP_COLON = 27
        UNKNOWN = 28

    def __init__(self, position, type, value=None):
        self.position = position
        self.type = type
        self.value = value


class TokenID(Token):
    def __init__(self, position, value):
        self.position = position
        self.type = Token.Type.ID
        self.value = value


class TokenConstraint(Token):
    class ConstraintType(Enum):
        STRING = 0
        NUMBER = 1
        BOOL = 2

    def __init__(self, position, value, type):
        self.position = position
        self.type = Token.Type.CONST
        self.value = value
        self.const_type = type


class TokenAddOperator(Token):
    class Value(Enum):
        PLUS = 0
        MINUS = 1

    def __init__(self, position, value):
        self.position = position
        self.type = Token.Type.OP_ADD
        self.value = value


class TokenMultOperator(Token):
    class Value(Enum):
        MULTIPLICATION = 0
        DIVISION = 1
        INT_DIVISION = 2
        REST_DIVISION = 3

    def __init__(self, position, value):
        self.position = position
        self.type = Token.Type.OP_MUL
        self.value = value


class TokenComparisonOperator(Token):
    class Value(Enum):
        EQUAL = 0
        GT = 1
        GT_EQ = 2
        LT = 3
        LT_EQ = 4
        NOT_EQUAL = 5
        
    def __init__(self, position, value):
        self.position = position
        self.type = Token.Type.OP_COMP
        self.value = value
